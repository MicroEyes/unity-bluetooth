﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.bluetooth
{
    public class CS_Java_Bridge
    {
        static AndroidJavaClass m_bluetoothManager;
        static AndroidJavaClass m_unityPlayer;
        static AndroidJavaObject m_unityActivity;
        
        internal CS_Java_Bridge()
        {

        }
       
        internal void Initialize(string a_uuid, int a_logType, int a_debugLevel)
        {
            m_bluetoothManager = new AndroidJavaClass("com.microeyes.bluetooth.BluetoothManager");
            m_unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_unityActivity = m_unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            m_bluetoothManager.CallStatic("Initialize", m_unityActivity, a_uuid, a_logType, a_debugLevel);
        }

        internal void Destroy()
        {
            m_bluetoothManager.CallStatic("Destroy");
        }
        internal bool IsSupported()
        {
            return m_bluetoothManager.CallStatic<bool>("IsBluetoothSupported");
        }

        internal bool IsEnable()
        {
            return m_bluetoothManager.CallStatic<bool>("IsEnable");
        }

        internal bool IsDiscoverable()
        {
            return m_bluetoothManager.CallStatic<bool>("IsDiscoverable");
        }

        internal void Enable()
        {
            m_bluetoothManager.CallStatic("Enable");
        }

        internal void EnableDiscoverable(int a_discoverableTime)
        {   
            m_bluetoothManager.CallStatic("EnableDiscoverable", a_discoverableTime);            
        }


        internal string GetPairedDevices()
        {
            return m_bluetoothManager.CallStatic<string>("GetPairedDevices");
        }

        internal string GetNewDiscoveredDevices()
        {
            return m_bluetoothManager.CallStatic<string>("GetNewDiscoveredDevices");            
        }

        internal string GetDiscoveredDevices()
        {       
            return m_bluetoothManager.CallStatic<string>("GetDiscoveredDevices");
        }

        internal bool StartDiscovery()
        {
            return m_bluetoothManager.CallStatic<bool>("StartDiscovery");
        }

        internal bool CancelDiscovery()
        {
            return m_bluetoothManager.CallStatic<bool>("CancelDiscovery");
        }

        internal void ShowDeviceList()
        {
            m_bluetoothManager.CallStatic("ShowDeviceList");
        }

        internal bool IsDiscovering()
        {
            return m_bluetoothManager.CallStatic<bool>("IsDiscovering");
        }

        internal void StartListening()
        {         
            m_bluetoothManager.CallStatic("StartListening");
        }

        internal void ConnectToDevice(string a_address)
        {
            m_bluetoothManager.CallStatic("Connect", a_address);
        }

        internal void SendText(string a_str)
        {
            m_bluetoothManager.CallStatic("SendText", a_str);
        }

        internal void Stop()
        {
            m_bluetoothManager.CallStatic("Stop");
        }

        internal void ShowToast(string a_msg)
        {
            m_bluetoothManager.CallStatic("ShowToast", a_msg);
        }

        internal void SendPacket(byte[] a_byteArray, bool a_isReflect)
        {
            m_bluetoothManager.CallStatic("SendPacket", a_byteArray, a_isReflect);
        }
        void Update()
        {
            if (m_bluetoothManager == null)
                return;

            byte[] l_byte = m_bluetoothManager.CallStatic<byte[]>("GetReflectedPacketData");
            if(l_byte != null)
            {
                BluetoothManager.OnReceived_ReflectedData(l_byte);
            }
        }

    }
}