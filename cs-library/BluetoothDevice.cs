﻿using UnityEngine;
using System;
using System.Collections;

namespace com.microeyes.bluetooth
{
    //TODO: Remove serializable
    [Serializable]
    public class BluetoothDevice
    {

        internal const string KEY_DEVICE = "device";
        internal const string KEY_NAME = "name";
        internal const string KEY_ADDRESS = "address";
        internal const string KEY_IS_PAIRED = "isPaired";
        internal const string KEY_DEVICE_LIST = "lstDevice";

        internal BluetoothDevice()
        {

        }

        [SerializeField]
        private string m_name = string.Empty;
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        [SerializeField]
        private string m_address = string.Empty;
        public string Address
        {
            get { return m_address; }
            set { m_address = value; }
        }

        [SerializeField]
        private bool m_isPaired = false;
        public bool IsPaired
        {
            get { return m_isPaired; }
            set { m_isPaired = value; }
        }

        public override string ToString()
        {
            return "Device: {Name:'" + Name + "', Address:'" + Address + "', IsPaired: '" + IsPaired + "'}";
        }

    }
}
