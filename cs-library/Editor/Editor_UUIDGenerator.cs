﻿using UnityEngine;
using System;
using System.Collections;
using UnityEditor;

public class Editor_UUIDGenerator : EditorWindow {

    static Editor_UUIDGenerator window;
    [MenuItem("Window/MicroEyes/Generate UUID")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        window = (Editor_UUIDGenerator)EditorWindow.GetWindow(typeof(Editor_UUIDGenerator));

        if(EditorPrefs.HasKey("bt_last_uuid"))
        {
            window.m_uuid = EditorPrefs.GetString("bt_last_uuid", string.Empty);
        }
    }

    string m_uuid = string.Empty;
    void OnGUI()
    {
        if(GUILayout.Button("Generate"))
        {
            Guid l_guid = System.Guid.NewGuid();
            m_uuid = l_guid.ToString();

            EditorPrefs.SetString("bt_last_uuid", m_uuid);            

            Debug.Log("New UUID Generated: ");
        }

        EditorGUILayout.TextField(m_uuid);
    }
	
}
