﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.bluetooth
{
    public class ResponseSimulator
    {
        private static ResponseSimulator s_instance = null;
        public static ResponseSimulator Instance
        {
            get 
            {
                if (s_instance == null)
                    s_instance = new ResponseSimulator();

                return s_instance; 
            }
        }

        string m_uuid = string.Empty;
        internal void Initialize(AndroidJavaObject a_unityActivity, string a_uuid)
        {
            string l_data = @"{""isSupported"":true, ""isEnabled"":true, ""isDiscoverable"":true, ""device"": { ""name"":""device_1"", ""address"":""2D:4S:5D:7S"", ""isPaired"":false }}";
            BluetoothManager.J2C_OnInitialized(l_data);
        }

        internal void Destroy()
        {
            Debug.Log("BluetoothManager::Destroyed");
        }

        internal void Enable()
        {
            string l_data = @"{""isEnabled"":true}";
            BluetoothManager.J2C_OnBluetoothEnabledByUser(l_data);
        }

        internal void EnableDiscoverable(int a_discoverableTime)
        {
            string l_data = @"{""isEnabled"":true}";
            BluetoothManager.J2C_OnBluetoothDiscoverableEnabledByUser(l_data);
        }

        internal string GetPairedDevices()
        {
            string l_data= @"{""lstDevice"": [ { ""name"":""device_1"", ""address"":""2D:4S:5D:7S"", ""isPaired"":true }, { ""name"":""device_2"", ""address"":""2D:4S:5D:7S"", ""isPaired"":true } ] }";
            return l_data;            
        }

        internal string GetNewDiscoveredDevices()
        {
            string l_data = @"{""lstDevice"": [ { ""name"":""device_new_1"", ""address"":""2D:4S:5D:7S"", ""isPaired"":true }, { ""name"":""device_new_2"", ""address"":""2D:4S:5D:7S"", ""isPaired"":true } ] }";
            return l_data;            
        }

        internal bool StartDiscovery()
        {
            return true;
        }

        internal bool CancelDiscovery()
        {
            return true;
        }

        internal object ShowDeviceList()
        {
            throw new System.NotImplementedException();
        }

        internal bool IsDiscovering()
        {
            throw new System.NotImplementedException();
        }

        internal void StartListerning()
        {
            Debug.Log("Started listening");
        }

        internal void ConnectToDevice(string a_address)
        {
            Debug.Log("Connecting to device: " + a_address);
        }

        internal void SendText(string a_str)
        {
            throw new System.NotImplementedException();
        }

        internal void SendPacket(byte[] a_byteArray, bool a_isReflect)
        {
            BluetoothManager.OnReceived_Packet(a_byteArray);
        }
    }
}
