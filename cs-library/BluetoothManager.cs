﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using com.microeyes.Data;
using com.microeyes.Serialization.Data;

namespace com.microeyes.bluetooth
{
    public class BluetoothManager
    {
        private static BluetoothManager Instance = null;

        private string UUID = string.Empty;

        static CS_Java_Bridge m_cs_java_bridge = null;
        internal static CS_Java_Bridge getInstance_CS_Java()
        {
            return m_cs_java_bridge;
        }

        private bool m_isServer = false;
        /// <summary>
        /// Returns TRUE, if this is server
        /// </summary>
        public static bool IsServer
        {
            get { return Instance.m_isServer; }
            internal set { Instance.m_isServer = value; }
        }

        public static BluetoothDevice MySelf = null;
        public static BluetoothDevice RemoteDevice = null;

        public delegate void Del_OnConnectionStateChanged(BluetoothDevice a_device, BluetoothMessageHandler.EConnectionState a_newState);

        internal const string KEY_IS_SUPPORTED = "isSupported";
        internal const string KEY_IS_ENABLED = "isEnabled";
        internal const string KEY_IS_DISCOVERABLE = "isDiscoverable";
        internal const string KEY_NEW_STATE = "newState";

        #region Events
        private Action<bool> m_eventOnEnable = null;
        public static Action<bool> OnEnabled
        {
            get { return Instance.m_eventOnEnable; }
            set 
            { 
                Instance.m_eventOnEnable = value;
                Debug.Log("BluetoothManager:: Callback registered for OnEnabled");
            }
        }

        private Action m_eventOnDisable = null;
        public static Action OnDisabled
        {
            get { return Instance.m_eventOnDisable; }
            set 
            {
                Instance.m_eventOnDisable = value;
                Debug.Log("BluetoothManager:: Callback registered for OnDisabled");
            }
        }


        private Action<bool> m_eventOnDiscoveryEnabled = null;

        public static Action<bool> OnDiscoverableEnabled
        {
            get { return Instance.m_eventOnDiscoveryEnabled; }
            set 
            { 
                Instance.m_eventOnDiscoveryEnabled = value;
                Debug.Log("BluetoothManager:: Callback registered for OnDiscoverableEnabled");
            }
        }

        private Action<EBluetoothState> m_eventOnStateChanged = null;

        public static Action<EBluetoothState> OnStateChanged
        {
            get { return Instance.m_eventOnStateChanged; }
            set 
            {
                Instance.m_eventOnStateChanged = value;
                Debug.Log("BluetoothManager:: Callback registered for OnStateChanged");
            }
        }

        private Action<EBluetoothDiscoverableState> m_eventOnDiscoverableStateChanged = null;
        public static Action<EBluetoothDiscoverableState> OnDiscoverableStateChanged
        {
            get { return Instance.m_eventOnDiscoverableStateChanged; }
            set 
            {
                Instance.m_eventOnDiscoverableStateChanged = value;
                Debug.Log("BluetoothManager:: Callback registered for OnDiscoverableStateChanged");
            }
        }

        private Action<BluetoothDevice> m_eventOnNewDeviceDiscovered = null;
        public static Action<BluetoothDevice> OnNewDeviceDiscovered
        {
            get { return Instance.m_eventOnNewDeviceDiscovered; }
            set 
            {
                Instance.m_eventOnNewDeviceDiscovered = value;
                Debug.Log("BluetoothManager:: Callback registered for OnNewDeviceDiscovered");
            }
        }

        private Action m_eventOnDiscoveryStarted = null;
        public static Action OnDiscoveryStarted
        {
            get { return Instance.m_eventOnDiscoveryStarted; }
            set 
            {
                Instance.m_eventOnDiscoveryStarted = value;
                Debug.Log("BluetoothManager:: Callback registered for OnDiscoveryStarted");
            }
        }

        private Action m_eventOnDiscoveryFinished = null;
        public static Action OnDiscoveryFinished
        {
            get { return Instance.m_eventOnDiscoveryFinished; }
            set 
            {
                Instance.m_eventOnDiscoveryFinished = value;
                Debug.Log("BluetoothManager:: Callback registered for OnDiscoveryFinished");
            }
        }

        private Del_OnConnectionStateChanged m_event_OnConnectionStateChanged = null;
        public static Del_OnConnectionStateChanged OnConnectionStateChanged
        {
            get { return Instance.m_event_OnConnectionStateChanged; }
            set 
            {
                Instance.m_event_OnConnectionStateChanged = value;
                Debug.Log("BluetoothManager:: Callback registered for OnConnectionStateChanged");
            }
        }

        private Action m_eventOnStartListeningToIncomingConnection = null;
        
        public static Action OnStartListeningToIncomingConnections
        {
            get { return Instance.m_eventOnStartListeningToIncomingConnection; }
            set 
            {
                Instance.m_eventOnStartListeningToIncomingConnection = value;
                Debug.Log("BluetoothManager:: Callback registered for OnStartListeningToIncomingConnections");
            }
        }

        private Action<BluetoothDevice> m_eventOnConnectionFailed = null;

        public static Action<BluetoothDevice> OnConnectionFailed
        {
            get { return Instance.m_eventOnConnectionFailed; }
            set 
            { 
                Instance.m_eventOnConnectionFailed = value;
                Debug.Log("BluetoothManager:: Callback registered for OnConnectionFailed");
            }
        }

        private Action<BluetoothDevice> m_eventOnConnectionEstablished = null;
        public static Action<BluetoothDevice> OnConnectionEstablished
        {
            get { return Instance.m_eventOnConnectionEstablished; }
            set 
            {
                Instance.m_eventOnConnectionEstablished = value;
                Debug.Log("BluetoothManager:: Callback registered for OnConnectionEstablished");
            }
        }

        private Action<BluetoothDevice> m_eventOnConnectionLost = null;
        public static Action<BluetoothDevice> OnConnectionLost
        {
            get { return Instance.m_eventOnConnectionLost; }
            set 
            {
                Instance.m_eventOnConnectionLost = value;
                Debug.Log("BluetoothManager:: Callback registered for OnConnectionLost");
            }
        }

        private Action<string> m_eventOnTextMessageSent = null;
        public static Action<string> OnTextMessageSent
        {
            get { return Instance.m_eventOnTextMessageSent; }
            set 
            {
                Instance.m_eventOnTextMessageSent = value;
                Debug.Log("BluetoothManager:: Callback registered for OnTextMessageSent");
            }
        }

        private Action<string> m_eventOnTextMessageReceived = null;
        public static Action<string> OnTextMessageReceived
        {
            get { return Instance.m_eventOnTextMessageReceived; }
            set 
            {
                Instance.m_eventOnTextMessageReceived = value;
                Debug.Log("BluetoothManager:: Callback registered for OnTextMessageReceived");
            }
        }

        private Action<BluetoothDevice> m_eventOnSelectedDeviceToConnect = null;
        public static Action<BluetoothDevice> OnSelectedDeviceToConnect
        {
            get { return Instance.m_eventOnSelectedDeviceToConnect; }
            set 
            {
                Instance.m_eventOnSelectedDeviceToConnect = value;
                Debug.Log("BluetoothManager:: Callback registered for OnSelectedDeviceToConnect");
            }
        }
        #endregion

        private Action<string> m_logger = null;
        public static Action<string> Logger
        {
            get { return Instance.m_logger; }
            set { Instance.m_logger = value; }
        }


        public enum EBluetoothState
        {
            STATE_DISCONNECTED = 0,
            STATE_CONNECTING = 1,
            STATE_CONNECTED = 2,
            STATE_DISCONNECTING = 3,
            STATE_OFF = 10,
            STATE_TURNING_ON = 11,
            STATE_ON = 12,
            STATE_TURNING_OFF = 13,

        }

        public enum EBluetoothDiscoverableState
        {
            SCAN_MODE_NONE = 20,
            SCAN_MODE_CONNECTABLE = 21,
            SCAN_MODE_CONNECTABLE_DISCOVERABLE = 23,
        }


        public const int DISCOVERABLE_TIME_DEFAULT = 120;
        public const int DISCOVERABLE_TIME_MIN = 0;
        public const int DISCOVERABLE_TIME_MAX = 3600;

        private bool m_isInitialized = false;
        public static bool IsInitialized
        {
            get 
            {
                if (Instance == null || !Instance.m_isInitialized)
                {
                    //Debug.LogError(ERROR_MSG_PLUGIN_NOT_INITIALIZED); ;
                    return false;
                }
                else
                {
                    return Instance.m_isInitialized;
                }
            }
        }
        
        private BluetoothManager()
        {
            
        }

        private const string ERROR_MSG_PLUGIN_NOT_INITIALIZED = "Bluetooth plugin not initialized. Please call BluetoothManager.Initialize() first.";
        private const string ERROR_MSG_INCORRECT_PLATFORM = "This Plugin only work on Android Devices.";
        private static bool IsRunningOnDevice
        {
            get
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    return true;
                }
                else
                {
                    //Debug.LogError(ERROR_MSG_INCORRECT_PLATFORM);
                    return false;
                }
            }
        }

        internal static string GameObjectName = "____BluetoothManager";

        internal static int LogType = 0;
        internal static int DebugLevel = 0;

        /// <summary>
        /// Initialize Bluetooth plugin
        /// </summary>
        /// <param name="a_uuid">Unique UUID of App</param>
        public static void Initialize(string a_uuid)
        {
            Initialize(a_uuid, BluetoothLog.NONE, BluetoothLog.DEBUG_NONE);
        }

        /// <summary>
        /// Initialize Bluetooth plugin
        /// </summary>
        /// <param name="a_uuid">Unique UUID of App</param>
        /// <param name="a_logType">LogType for plugin</param>
        public static void Initialize(string a_uuid, int a_logType)
        {
            Initialize(a_uuid, a_logType, BluetoothLog.DEBUG_NONE);
        }

        /// <summary>
        /// Initialize Bluetooth plugin
        /// </summary>
        /// <param name="a_uuid">Unique UUID of App</param>
        /// <param name="a_logType">LogType for plugin</param>
        /// <param name="a_debugLevel">Set DebugLevel. Only applicable if Type 'Debug' is Set.</param>
        public static void Initialize(string a_uuid, int a_logType, int a_debugLevel)
        {
            LogType = a_logType;
            DebugLevel = a_debugLevel;
            if (!BluetoothManager.IsRunningOnDevice)
                return;

            GameObject l_oldGameObject = GameObject.Find(GameObjectName);
            if (l_oldGameObject)
            {
                Debug.Log("BluetoothManager:: Deleting Old Object");
#if UNITY_EDITOR
                GameObject.DestroyImmediate(l_oldGameObject);
#else
                GameObject.Destroy(l_oldGameObject);
#endif
            }

            Instance = new BluetoothManager();
            m_cs_java_bridge = new CS_Java_Bridge();

            Instance.UUID = a_uuid;

            GameObject l_go = new GameObject(GameObjectName);
            l_go.AddComponent<Java_CS_Bridge>();

            Instance.m_isInitialized = false;
            m_cs_java_bridge.Initialize(a_uuid, a_logType, a_debugLevel);
        }

        //static void GetMySelf()
        //{
        //    if (!BluetoothManager.IsRunningOnDevice)
        //        return;

        //    string l_data = m_cs_java_bridge.GetMySelf();

        //    JSONNode l_node = JSON.Parse(l_data);
        //    MySelf = JSONToBluetoothDevice(l_node);
        //}

        /// <summary>
        /// Both Local & Remote device can go into LISTEN mode before connect. both have 'IsStartedListerningAsServer' TRUE, after clicking LISTEN.
        /// BUT device which call Connect(ADDRESS), which would be client, sets 'IsStartedListerningAsServer' to FALSE.
        /// IF CONNECTION_FAILS OR CONNECTION_LOST, both device goes into LISTEN mode & again, 'IsStartedListerningAsServer' sets to TRUE. 
        /// IF CONNECTION_ESTABLISHED, only one device have 'IsStartedListerningAsServer' to TRUE.
        /// </summary>
        private bool m_isStartedListeningAsServer = false;
        public static bool IsStartedListeningAsServer
        {
            get { return Instance.m_isStartedListeningAsServer; }
            set 
            {
                Debug.LogError("m_isStartedListeningAsServer: Existing: " + Instance.m_isStartedListeningAsServer + ", New: " + value);
                Instance.m_isStartedListeningAsServer = value; 
            }
        }

        public static void Destroy()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;

            m_cs_java_bridge.Destroy();
        }

        private static bool s_isSupported = false;
        public static bool IsSupported
        {
            get
            {
                if (!BluetoothManager.IsRunningOnDevice)
                    return false;

                if (Instance != null && Instance.m_isInitialized)
                {
                    return s_isSupported;
                }
                else
                {
                    return Instance.IsSupported_ForceCheck();
                }
            }
            private set {  s_isSupported = value; }
        }

        private static bool s_isEnable = false;
        public static bool IsEnable
        {
            get
            {
                if (!BluetoothManager.IsRunningOnDevice)
                    return false;

                if (Instance != null && Instance.m_isInitialized)
                {
                    return s_isEnable;
                }
                else
                {
                    return Instance.IsEnabled_ForceCheck();
                }
            }
            private set { s_isEnable = value; }
        }

        private static bool s_isDiscoverable = false;
        public static bool IsDiscoverable
        {
            get
            {
                if (!BluetoothManager.IsRunningOnDevice)
                    return false;

                if (Instance != null && Instance.m_isInitialized)
                {
                    return s_isDiscoverable;
                }
                else
                {
                    return Instance.IsDiscoverable_ForceCheck();
                }
            }
            private set { s_isDiscoverable = value; }
        }
        
        public bool IsSupported_ForceCheck()
        {
            Debug.Log("BluetoothManager::IsSupported_ForceCheck");
            
            if (!BluetoothManager.IsRunningOnDevice || Instance == null)
                return false;

            return m_cs_java_bridge.IsSupported();
        }

        public bool IsEnabled_ForceCheck()
        {
            Debug.Log("BluetoothManager::IsEnabled_ForceCheck");

            if (!BluetoothManager.IsRunningOnDevice || Instance == null)
                return false;

            return m_cs_java_bridge.IsEnable();
        }

        public bool IsDiscoverable_ForceCheck()
        {
            Debug.Log("BluetoothManager::IsDiscoverable_ForceCheck");

            if (!BluetoothManager.IsRunningOnDevice || Instance == null)
                return false;

            return m_cs_java_bridge.IsDiscoverable(); 
        }

        /// <summary>
        /// Enable Bluetooth Adapter.
        /// If not Enabled, Permission UI will get shown from device
        /// If already Enabled, then immediate callback returned
        /// </summary>
        public static void Enable()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            m_cs_java_bridge.Enable();
        }

        /// <summary>
        /// Enable Discoverable Mode, with default discoverable time of 120secs
        /// </summary>
        public static void EnableDiscoverable()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            EnableDiscoverable(BluetoothManager.DISCOVERABLE_TIME_DEFAULT);
        }

        /// <summary>
        /// Enable Discoverable Mode
        /// </summary>
        /// <param name="a_discoverableTime">Discoverable time. Default:120secs; Min: 0secs, Always discoverable; Max: 3600secs. Any value beyond MIN & MAX, leads to DEFAULT TIME(120s)</param>
        public static void EnableDiscoverable(int a_discoverableTime)
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            m_cs_java_bridge.EnableDiscoverable(a_discoverableTime);
        }

        public static List<BluetoothDevice> GetPairedDevices()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return null;

            List<BluetoothDevice> l_lstDevice = new List<BluetoothDevice>();

            string l_strPairedDevices = m_cs_java_bridge.GetPairedDevices();

            Debug.Log("BluetoothManager:: GetPairedDevices:: data: " + l_strPairedDevices);

            //JSONNode l_data = JSON.Parse(l_strPairedDevices);

            l_lstDevice = JSONToBluetoothDeviceList(JSON.Parse(l_strPairedDevices));

            if (l_lstDevice == null)
            {
                Debug.LogError("BluetoothManager:: GetPairedDevices:: 'lstDevice' key not received");
            }
            else
            {
                Debug.Log("BluetoothManager:: GetPairedDevices:: Count: " + l_lstDevice.Count);
            }

            return l_lstDevice;
        }

        public static List<BluetoothDevice> GetAllDevices()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return null;

            List<BluetoothDevice> l_lstDevices = null;

            string l_strAllDevices = m_cs_java_bridge.GetDiscoveredDevices();

            Debug.Log("BluetoothManager:: GetDiscoveredDevices:: data: " + l_strAllDevices);

            l_lstDevices = JSONToBluetoothDeviceList(JSON.Parse(l_strAllDevices));

            if (l_lstDevices == null)
            {
                Debug.LogError("BluetoothManager:: GetAllDevices:: 'lstDevice' key not received");
            }
            else
            {
                Debug.Log("BluetoothManager:: GetAllDevices:: Count: " + l_lstDevices.Count);
            }

            return l_lstDevices;
        }

        public static void StartDiscovery()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;

            m_cs_java_bridge.StartDiscovery();
        }

        public static void ShowDeviceList()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;

            m_cs_java_bridge.ShowDeviceList();
        }

        public static bool IsDiscovering
        {
            get 
            {
                if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                    return false;
                else
                    return m_cs_java_bridge.IsDiscovering(); 
            }
        }

        public static void StartListening()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            m_cs_java_bridge.StartListening();
        }

        public static void Connect(BluetoothDevice a_device)
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            IsStartedListeningAsServer = false;
            m_cs_java_bridge.ConnectToDevice(a_device.Address);
        }

        public static void SendText(String a_str)
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            m_cs_java_bridge.SendText(a_str);
        }

        public static void Stop()
        {
            if (!BluetoothManager.IsRunningOnDevice || !BluetoothManager.IsInitialized)
                return;
            m_cs_java_bridge.Stop();
        }

        public delegate void Del_RequestHandler(IDataObject a_data);

        /// <summary>
        /// Dictionary keeps record of all registered requests
        /// </summary>
        static Dictionary<string, Del_RequestHandler> s_requestHolder = new Dictionary<string, Del_RequestHandler>();
        public static void AddResponseHandler(string a_requestId, Del_RequestHandler a_callback)
        {
            if (s_requestHolder.ContainsKey(a_requestId))
            {
                Del_RequestHandler l_callback = s_requestHolder[a_requestId];
                s_requestHolder.Remove(a_requestId);
                l_callback += a_callback;

                s_requestHolder.Add(a_requestId, l_callback);
            }
            else
            {
                s_requestHolder.Add(a_requestId, a_callback);
            }
        }

        public static bool RemoveResponseHandler(string a_requestId, Del_RequestHandler a_callback)
        {
            if (s_requestHolder.ContainsKey(a_requestId))
            {
                s_requestHolder.Remove(a_requestId);
                return true;
            }
            else
            {
                return false;
            }
        }

        public const string KEY_REQUEST_ID = "req_id";
        public const string KEY_IS_SENT_BY_ME = "is_by_me";
        public const string KEY_USER_DATA = "usr_data";

        public static void Send(string a_requestId, IDataObject a_data)
        {
            Send(a_requestId, a_data, false);
        }

        public static void Send(string a_requestId, IDataObject a_data, bool a_isReflect)
        {
            IDataObject l_parent = new DataObject();
            l_parent.PutString(BluetoothManager.KEY_REQUEST_ID, a_requestId);
            l_parent.PutString(BluetoothDevice.KEY_ADDRESS, MySelf.Address);
            l_parent.PutDataObject(BluetoothManager.KEY_USER_DATA, a_data);

            ByteData l_byteData = l_parent.ToByteData();

            if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
            {
                Debug.Log("SendingPacket: " + l_parent.Dump());
            }

            //NetworkHandler.Instance.LsttLogs.Add("Dump: " + l_parent.Dump() + ", Size: " + l_byteData.DataBuffer.Length);

            if (Logger != null)
            {
                Logger.Invoke("Dump: " + l_parent.Dump() + ", Size: " + l_byteData.DataBuffer.Length);
            }


            m_cs_java_bridge.SendPacket(l_byteData.DataBuffer, a_isReflect);
        }

        public static void OnReceived_ReflectedData(byte[] a_byteArray)
        {
            OnReceived_Packet(a_byteArray);
        }

        public static void OnReceived_Packet(byte[] a_byteArray)
        {
            IDataObject l_parent = new DataObject();
            ByteData l_btyeData = new ByteData(a_byteArray);
            l_parent = DataObject.ByteDataToObject(l_btyeData);

            string l_address = l_parent.GetString(BluetoothDevice.KEY_ADDRESS);
            l_parent.Remove(BluetoothDevice.KEY_ADDRESS);
            l_parent.PutBool(BluetoothManager.KEY_IS_SENT_BY_ME, l_address.Equals(BluetoothManager.MySelf.Address));

            if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && DebugLevel == BluetoothLog.DEBUG_HIGH)
            {
                Debug.Log("Received: " + l_parent.Dump());
            }

            TriggerResponse(l_parent);
        }

        public static void TriggerResponse(IDataObject a_dataObject)
        {
            string l_requestID = a_dataObject.GetString(BluetoothManager.KEY_REQUEST_ID);

            if(s_requestHolder.ContainsKey(l_requestID))
            {
                Del_RequestHandler l_callback = s_requestHolder[l_requestID];
                if(l_callback != null)
                {
                    l_callback.Invoke(a_dataObject);
                }
                else
                {
                    Debug.LogError("Response callback function is null for request id '" + l_requestID + "'");
                }
            }
            else
            {
                Debug.LogError("Response received for request id '" + l_requestID + "'. But no callback registered. Please register callback for request id '" + l_requestID + "'");
            }
        }

        internal static BluetoothDevice JSONToBluetoothDevice(JSONNode l_jsonObj)
        {
            BluetoothDevice l_newDevice = new BluetoothDevice();
            l_newDevice.Name = l_jsonObj[BluetoothDevice.KEY_NAME];
            l_newDevice.Address = l_jsonObj[BluetoothDevice.KEY_ADDRESS];
            l_newDevice.IsPaired = l_jsonObj[BluetoothDevice.KEY_IS_PAIRED].AsBool;

            return l_newDevice;
        }

        internal static List<BluetoothDevice> JSONToBluetoothDeviceList(JSONNode l_jsonObj)
        {
            List<BluetoothDevice> l_lstDevices = null;
            var l_arry = l_jsonObj[BluetoothDevice.KEY_DEVICE_LIST];

            if (l_arry != null)
            {
                l_lstDevices = new List<BluetoothDevice>();
                for (int l_index = 0; l_index < int.MaxValue; l_index++)
                {
                    Debug.Log("Processing Index: " + l_index);
                    if (l_arry[l_index] == null)
                    {
                        Debug.Log("NULL found @" + l_index);
                        break;
                    }
                    else
                    {
                        BluetoothDevice l_device = JSONToBluetoothDevice(l_arry[l_index]);

                        l_lstDevices.Add(l_device);
                        Debug.Log("Device Added @index: " + l_index);
                    }
                }
            }
            else
            {
                Debug.Log("BluetoothManager:: GetPairedDevices:: 'lstDevice' key not received");
            }

            return l_lstDevices;
        }

        internal static void ShowToast(string a_message)
        {
            m_cs_java_bridge.ShowToast(a_message);
        }

        #region Java-CS

        internal static void J2C_OnInitialized(string a_data)
        {
            Debug.Log("BluetoothManager::J2C_OnInitialized:: Data: " + a_data);

            Instance.m_isInitialized = true;

            JSONNode l_data = JSON.Parse(a_data);
            
            if(l_data[KEY_IS_SUPPORTED] != null)
            {
                IsSupported = l_data[KEY_IS_SUPPORTED].AsBool;
                if (IsSupported)
                {
                    IsEnable = l_data[KEY_IS_ENABLED].AsBool;
                    IsDiscoverable = l_data[KEY_IS_DISCOVERABLE].AsBool;
                    MySelf = JSONToBluetoothDevice(l_data[BluetoothDevice.KEY_DEVICE]);
                }
                else
                {
                    IsSupported = false;
                    IsEnable = false;
                    IsDiscoverable = false;
                    MySelf = null;
                }
            }

            Debug.Log("Initialized:: IsSupported: " + IsSupported + ", IsEnable: " + IsEnable + ", IsDiscoverable: " + IsDiscoverable + ", MyDevice: " + ((MySelf == null) ? "null" : MySelf.ToString()));
        }

        internal static void J2C_OnBluetoothEnabledByUser(string a_data)
        {
            Debug.Log("BluetoothManager:: J2C_OnBluetoothEnabledByUser:: Data: " + a_data);

            var l_data = JSON.Parse(a_data);

            if (l_data[KEY_IS_ENABLED] != null)
            {
                IsEnable = l_data[KEY_IS_ENABLED].AsBool;

                if(OnEnabled != null)
                {
                    OnEnabled.Invoke(IsEnable);
                }
                else
                {
                    Debug.LogError("BluetoothManager:: J2C_OnBluetoothEnabledByUser:: No callback registered.");
                }
            }
            else
            {
                Debug.Log("BluetoothManager:: J2C_OnBluetoothEnabledByUser:: 'KEY_IS_ENABLED' key not received");
            }
        }

        internal static void J2C_OnBluetoothStateChanged(string a_data)
        {
            Debug.Log("BluetoothManager:: J2C_OnBluetoothStateChanged:: Data: " + a_data);

            var l_data = JSON.Parse(a_data);
            if(l_data[KEY_NEW_STATE] != null)
            {
                int l_newState = l_data[KEY_NEW_STATE].AsInt;
                EBluetoothState l_state = (EBluetoothState)l_newState;
                if (OnStateChanged != null)
                {
                    OnStateChanged.Invoke(l_state);
                }
                else
                {
                    Debug.LogError("BluetoothManager:: J2C_OnBluetoothStateChanged:: OnStateChanged:: No callback registered.");
                }

                if(l_state == EBluetoothState.STATE_ON)
                {
                    IsEnable = true;
                }
                else
                {
                    IsEnable = false; //Set FALSE in other STATES
                }

                if(l_state == EBluetoothState.STATE_OFF)
                {
                    if (OnDisabled != null)
                    {
                        OnDisabled.Invoke();
                    }
                    else
                    {
                        Debug.LogError("BluetoothManager:: J2C_OnBluetoothStateChanged:: OnDisabled:: No callback registered");
                    }
                }

            }
            else
            {
                Debug.LogError("BluetoothManager:: J2C_OnBluetoothStateChanged:: 'KEY_NEW_STATE' key not received");
            }

        }

        internal static void J2C_OnBluetoothDiscoverableEnabledByUser(string a_data)
        {
            Debug.Log("BluetoothManager:: J2C_OnBluetoothDiscoverableEnabledByUser:: Data: " + a_data);

            var l_data = JSON.Parse(a_data);

            if (l_data[KEY_IS_ENABLED] != null)
            {
                IsDiscoverable = l_data[KEY_IS_ENABLED].AsBool;

                if (OnDiscoverableEnabled != null)
                {
                    OnDiscoverableEnabled.Invoke(IsDiscoverable);
                }
                else
                {
                    Debug.LogError("BluetoothManager:: J2C_OnBluetoothDiscoverableEnabledByUser:: No callback registered");
                }
            }
            else
            {
                Debug.Log("BluetoothManager:: J2C_OnBluetoothDiscoverableEnabledByUser:: 'KEY_IS_ENABLED' key not received");
            }
        }

        internal static void J2C_OnBluetoothDiscoverableStateChanged(string a_data)
        {
            Debug.Log("BluetoothManager:: J2C_OnBluetoothDiscoverableStateChanged:: Data: " + a_data);

            var l_data = JSON.Parse(a_data);

            if (l_data[KEY_NEW_STATE] != null)
            {
                int l_newState = l_data[KEY_NEW_STATE].AsInt;
                EBluetoothDiscoverableState l_state = (EBluetoothDiscoverableState)l_newState;
                if(OnDiscoverableStateChanged != null)
                {
                    OnDiscoverableStateChanged(l_state);
                }
                else
                {
                    Debug.LogError("BluetoothManager:: J2C_OnBluetoothDiscoverableStateChanged:: No callback registered");
                }

                if(l_state == EBluetoothDiscoverableState.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
                {
                    IsDiscoverable = true;
                }
                else
                {
                    IsDiscoverable = false;   //Set FALSE in other STATES
                }

            }
            else
            {
                Debug.LogError("BluetoothManager:: J2C_OnBluetoothDiscoverableStateChanged:: 'KEY_NEW_STATE' key not received");
            }
        }
        
        internal static void J2C_OnDiscoveryStarted()
        {
            Debug.Log("BluetoothManager::J2C_OnDiscoveryStarted");
            if (OnDiscoveryStarted != null)
            {
                OnDiscoveryStarted.Invoke();
            }
            else
            {
                Debug.LogError("BluetoothManager:: J2C_OnDiscoveryStarted:: No callback registered");
            }
        }

        internal static void J2C_OnDiscoveryFinished()
        {
            Debug.Log("BluetoothManager::J2C_OnDiscoveryFinished");
            if(OnDiscoveryFinished != null)
            {
                OnDiscoveryFinished.Invoke();
            }
            else
            {
                Debug.LogError("BluetoothManager:: J2C_OnDiscoveryFinished:: No callback registered");
            }
        }

        internal static void J2C_OnNewDeviceDiscovered(string a_data)
        {
            Debug.Log("BluetoothManager:: J2C_OnNewDeviceDiscovered: Data: " + a_data);

            var l_data = JSON.Parse(a_data);
            
            if(l_data[BluetoothDevice.KEY_NAME] != null)
            {
                Debug.Log("BluetoothManager:: Found 'name'");
                BluetoothDevice l_newDevice = JSONToBluetoothDevice(l_data);

                Debug.Log("BluetoothManager:: Found device: " + l_newDevice.ToString());
                if (OnNewDeviceDiscovered != null)
                {
                    Debug.Log("BluetoothManager:: Callback");
                    OnNewDeviceDiscovered.Invoke(l_newDevice);
                }
                else
                {
                    Debug.LogError("BluetoothManager:: J2C_OnNewDeviceDiscovered:: No callback registered");
                }
            }
            else
            {
                Debug.LogError("BluetoothManager:: J2C_OnNewDeviceDiscovered:: 'KEY_NAME' key not received");
            }
        }

        internal static void J2C_OnSelectedDeviceToConnect(string a_data)
        {
            Debug.Log("BluetoothManager:: J2C_OnSelectedDeviceToConnect: " + a_data);

            var l_data = JSON.Parse(a_data);
            if(l_data[BluetoothDevice.KEY_ADDRESS] != null)
            {
                BluetoothDevice l_deviceToConnect = JSONToBluetoothDevice(l_data);

                if(OnSelectedDeviceToConnect != null)
                {
                    OnSelectedDeviceToConnect.Invoke(l_deviceToConnect);
                }
                else
                {
                    Debug.LogError("BluetoothManager:: J2C_OnSelectedDeviceToConnect:: No callback registered");
                }
            }
            else
            {
                Debug.LogError("BluetoothManager:: J2C_OnNewDeviceDiscovered:: 'KEY_ADDRESS' key not received");
            }
        }

        internal static void J2C_OnMessageTypeReceived(string a_data)
        {
            if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
            {
                Debug.Log("BluetoothManager:: J2C_OnMessageTypeReceived: " + a_data);
            }

            BluetoothMessageHandler.OnReceivedMessage(a_data);
        }

        internal static void J2C_OnDataReceived(string a_data)
        {
            if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
            {
                Debug.Log("BluetoothManager:: J2C_OnDataReceived: " + a_data);
            }

            BluetoothMessageHandler.OnDataReceived(a_data);
        }

        #endregion

        internal static string GetDiscoveredDevices()
        {
            throw new NotImplementedException();
        }
    }
}
