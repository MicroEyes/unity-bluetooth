﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.bluetooth
{
    public class Java_CS_Bridge : MonoBehaviour
    {
        void OnInitialized(string a_data)
        {
            Debug.Log("J2C::OnInitialized");
            BluetoothManager.J2C_OnInitialized(a_data);
        }

        void OnEnabledByUser(string a_data)
        {
            Debug.Log("J2C::OnBluetoothEnabledByUser");
            BluetoothManager.J2C_OnBluetoothEnabledByUser(a_data);
        }

        void OnStateChanged(string a_data)
        {
            Debug.Log("J2C::OnBluetoothStateChanged");
            BluetoothManager.J2C_OnBluetoothStateChanged(a_data);
        }

        void OnDiscoverableEnabledByUser(string a_data)
        {
            Debug.Log("J2C::OnBluetoothDiscoverableEnabledByUser");
            BluetoothManager.J2C_OnBluetoothDiscoverableEnabledByUser(a_data);
        }

        void OnDiscoverableStateChanged(string a_data)
        {
            Debug.Log("J2C::OnBluetoothDiscoverableStateChanged");
            BluetoothManager.J2C_OnBluetoothDiscoverableStateChanged(a_data);
        }

        void OnDiscoveryStarted(string a_data)
        {
            Debug.Log("J2C::OnDiscoveryStarted");
            BluetoothManager.J2C_OnDiscoveryStarted();
        }

        void OnDiscoveryFinished(string a_data)
        {
            Debug.Log("J2C::OnDiscoveryFinished");
            BluetoothManager.J2C_OnDiscoveryFinished();
        }

        void OnNewDeviceDiscovered(string a_data)
        {
            Debug.Log("J2C:: OnFoundNewDevice");
            BluetoothManager.J2C_OnNewDeviceDiscovered(a_data);
        }

        void OnSelectedDeviceToConnect(string a_address)
        {
            Debug.Log("OnSelectedDeviceToConnect");
            BluetoothManager.J2C_OnSelectedDeviceToConnect(a_address);
        }

        void OnConnectionStateChanged(string a_data)
        {

        }

        void OnMessageTypeReceived(string a_data)
        {
            BluetoothManager.J2C_OnMessageTypeReceived(a_data);
        }

        void OnDataReceived(string a_data)
        {
            BluetoothManager.J2C_OnDataReceived(a_data);
        }

    }
}
