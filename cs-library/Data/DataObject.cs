﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using com.microeyes.Serialization;
using com.microeyes.Serialization.Data;

namespace com.microeyes.Data
{
    [Serializable]
    public class DataObject : IDataObject
    {
        private Dictionary<string, DataWrapper> m_dataHolder = new Dictionary<string, DataWrapper>();

        private const char CHAR_INDENT_OPEN = '{';
        private const char CHAR_INDENT_CLOSE = '}';
        private const char CHAR_DIVIDER = ';';
        private const char CHAR_NEW_LINE = '\n';
        private const char TAB = '\t';
        private const char DOT = '.';

        private DefaultDataSerializer m_serializer = null;

        public DataObject ()
        {
            m_serializer = DefaultDataSerializer.Instance;
        }

        public string Dump()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(Convert.ToString(CHAR_INDENT_OPEN));
            foreach (KeyValuePair<string, DataWrapper> keyValuePair in this.m_dataHolder)
            {
                DataWrapper l_data = keyValuePair.Value;
                string key = keyValuePair.Key;
                EDataType l_type = l_data.EType;
                stringBuilder.Append("(" + ((object)(EDataType)l_type).ToString().ToLower() + ")");
                stringBuilder.Append(" " + key + ": ");
                if (l_type == EDataType.DataObject)
                    stringBuilder.Append((l_data.Data as DataObject).Dump());
                /*else if (type == 17)
                    stringBuilder.Append((sfsDataWrapper.Data as SFSArray).GetDump(false));*/
                else if (l_type > EDataType.String && l_type < EDataType.DataObject)
                    stringBuilder.Append("[" + l_data.Data + "]");
                else
                    stringBuilder.Append(l_data.Data);

                stringBuilder.Append(CHAR_DIVIDER);
            }
            string str = ((object)stringBuilder).ToString();
            if (this.Count > 0)
                str = str.Substring(0, str.Length - 1);
            return str + (object)CHAR_INDENT_CLOSE;
        }

        #region Formatting
        public static string PrettyPrint(string a_strDump)
        {
            StringBuilder l_stringBuilder = new StringBuilder();
            int l_count = 0;
            for (int l_index = 0; l_index < a_strDump.Length; ++l_index)
            {
                char ch = a_strDump[l_index];
                if ((int)ch == (int)CHAR_INDENT_OPEN)
                {
                    ++l_count;
                    l_stringBuilder.Append(CHAR_NEW_LINE);
                    l_stringBuilder.Append((object)GetFormatTabs(l_count));
                }
                else if ((int)ch == (int)CHAR_INDENT_CLOSE)
                {
                    --l_count;

                    l_stringBuilder.Append(CHAR_NEW_LINE + GetFormatTabs(l_count));
                }
                else if ((int)ch == (int)CHAR_DIVIDER)
                    l_stringBuilder.Append(CHAR_NEW_LINE + GetFormatTabs(l_count));
                else
                    l_stringBuilder.Append(ch);
            }

            return ((object)l_stringBuilder).ToString();
        }

        private static string GetFormatTabs(int a_tabCount)
        {
            return FillString(TAB, a_tabCount);
        }
        private static string FillString(char a_char, int a_count)
        {
            StringBuilder l_stringBuilder = new StringBuilder();
            for (int l_charAppendIndex = 0; l_charAppendIndex < a_count; ++l_charAppendIndex)
                l_stringBuilder.Append(a_char);
            return ((object)l_stringBuilder).ToString();
        }
        #endregion

        #region PUT
        public void Put(string a_key, DataWrapper a_data)
        {
            m_dataHolder.Add(a_key, a_data);
        }
        public void PutBool(string a_key, bool a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Bool, a_value));
        }

        public void PutByte(string a_key, byte a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Byte, a_value));
        }
        public void PutShort(string a_key, short a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Short, a_value));
        }

        public void PutInt(string a_key, int a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Int, a_value));
        }
        public void PutLong(string a_key, long a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Long, a_value));
        }

        public void PutFloat(string a_key, float a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Float, a_value));
        }
        public void PutDouble(string a_key, double a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Double, a_value));
        }
        public void PutString(string a_key, string a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.String, a_value));
        }

        public void PutBoolArray(string a_key, bool[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Bool_Array, a_value));
        }

        public void PutByteArray(string a_key, byte[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Byte_Array, a_value));
        }
        public void PutShortArray(string a_key, short[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Short_Array, a_value));
        }

        public void PutIntArray(string a_key, int[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Int_Array, a_value));
        }
        public void PutLongArray(string a_key, long[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Long_Array, a_value));
        }

        public void PutFloatArray(string a_key, float[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Float_Array, a_value));
        }
        public void PutDoubleArray(string a_key, double[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.Double_Array, a_value));
        }
        public void PutStringArray(string a_key, string[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.String_Array, a_value));
        }

        public void PutDataObject(string a_key, IDataObject a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.DataObject, a_value));
        }

        public void PutDataArray(string a_key, DataObject[] a_value)
        {
            m_dataHolder.Add(a_key, new DataWrapper(EDataType.DataArray, a_value));
        }

        #endregion

        #region GET
        public bool GetBool(string a_key)
        {
            return GetValue<bool>(a_key);
        }
        public byte GetByte(string a_key)
        {
            return GetValue<byte>(a_key);
        }
        public short GetShort(string a_key)
        {
            return GetValue<short>(a_key);
        }

        public int GetInt(string a_key)
        {
            return GetValue<int>(a_key);
        }
        public long GetLong(string a_key)
        {
            return GetValue<long>(a_key);
        }

        public float GetFloat(string a_key)
        {
            return GetValue<float>(a_key);
        }
        public double GetDouble(string a_key)
        {
            return GetValue<double>(a_key);
        }

        public string GetString(string a_key)
        {
            return GetValue<string>(a_key);
        }

        public bool[] GetBoolArray(string a_key)
        {
            return GetValue<bool[]>(a_key);
        }
        public byte[] GetByteArray(string a_key)
        {
            return GetValue<byte[]>(a_key);
        }
        public short[] GetShortArray(string a_key)
        {
            return GetValue<short[]>(a_key);
        }

        public int[] GetIntArray(string a_key)
        {
            return GetValue<int[]>(a_key);
        }
        public long[] GetLongArray(string a_key)
        {
            return GetValue<long[]>(a_key);
        }

        public float[] GetFloatArray(string a_key)
        {
            return GetValue<float[]>(a_key);
        }
        public double[] GetDoubleArray(string a_key)
        {
            return GetValue<double[]>(a_key);
        }

        public string[] GetStringArray(string a_key)
        {
            return GetValue<string[]>(a_key);
        }

        public IDataObject GetDataObject(string a_key)
        {
            return GetValue<IDataObject>(a_key);
        }

        public DataWrapper GetData(string a_key)
        {
            return m_dataHolder[a_key];
        }
        #endregion

        private T GetValue<T>(string a_key)
        {
            if (!m_dataHolder.ContainsKey(a_key))
            {
                return default(T);
            }
            else
            {
                return (T)m_dataHolder[a_key].Data;
            }
        }
        
        public int Count
        {
            get { return m_dataHolder.Count; }
        }

        public bool Remove(string a_key)
        {
            if (m_dataHolder.ContainsKey(a_key))
            {
                m_dataHolder.Remove(a_key);
                return true;
            }

            return false;
        }

        public bool Contains(string a_key)
        {
            return m_dataHolder.ContainsKey(a_key);
        }

        public string[] GetKeys()
        {
            string[] l_keys = new string[m_dataHolder.Keys.Count];
            m_dataHolder.Keys.CopyTo(l_keys, 0);
            return l_keys;
        }

        public bool IsNull(string a_key)
        {
            return !m_dataHolder.ContainsKey(a_key) || (m_dataHolder[a_key] == null);
        }

        public ByteData ToByteData()
        {
            return m_serializer.ObjectToByteArray(this);
        }

        public object this[string a_key]
        {
            get
            {
                if (m_dataHolder.ContainsKey(a_key))
                    return m_dataHolder[a_key];
                else
                    return null;   
            }
        }
        public static DataObject ByteDataToObject(ByteData a_byteData)
        {
            return DefaultDataSerializer.Instance.ByteArrayToObject(a_byteData) as DataObject;
        }
    }
}