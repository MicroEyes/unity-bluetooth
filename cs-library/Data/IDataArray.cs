﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.Data
{
    interface IDataArray
    {
        bool Contains(object a_value);
        object GetElementAt(int a_index);
        object RemoveElementAt(int a_index);
        int Count { get; }

        void AddBool(bool a_value);
        void AddByte(byte a_value);
        void AddShort(short a_value);
        void AddInt(int a_value);
        void AddLong(long a_value);
        void AddFloat(float a_value);
        void AddDouble(double a_value);
        void AddString(string a_value);
        void AddBoolArray(bool[] a_value);
        void AddByteArray(byte[] a_value);
        void AddShortArray(short[] a_value);
        void AddIntArray(int[] a_value);
        void AddLongArray(long[] a_value);
        void AddFloatArray(float[] a_value);
        void AddDoubleArray(double[] a_value);
        void AddStringArray(string[] a_value);

        bool GetBool(int a_index);
        byte GetByte(int a_index);
        short GetShort(int a_index);
        int GetInt(int a_index);
        long GetLong(int a_index);
        float GetFloat(int a_index);
        double GetDouble(int a_index);
        string GetString(int a_index);
        bool[] GetBoolArray(int a_index);
        byte[] GetByteArray(int a_index);
        short[] GetShortArray(int a_index);
        int[] GetIntArray(int a_index);
        long[] GetLongArray(int a_index);
        float[] GetFloatArray(int a_index);
        double[] GetDoubleArray(int a_index);
        string[] GetStringArray(int a_index);        

    }
}
