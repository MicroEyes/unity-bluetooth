﻿using UnityEngine;
using System;
using System.Collections;

namespace com.microeyes.Data
{
    public enum EDataType
    {
        Null = 0,
        Bool = 1,
        Byte = 2,
        Short = 3,
        Int = 4,
        Long = 5,
        Float = 6,
        Double = 7,
        String = 8,
        Bool_Array = 9,
        Byte_Array = 10,
        Short_Array = 11,
        Int_Array = 12,
        Long_Array = 13,
        Float_Array = 14,
        Double_Array = 15,
        String_Array = 16,
        DataObject = 17,
        DataArray = 18
    }

    [Serializable]
    public class DataWrapper
    {
        private int m_type = -1;
        private object m_data = null;

        public int Type
        {
            get { return m_type; }
        }

        internal EDataType EType
        {
            get { return (EDataType)Type; }
        }

        public object Data
        {
            get { return m_data; }
        }

        public DataWrapper(EDataType a_type, object a_data)
        {
            m_type = (int)a_type;
            m_data = a_data;
        }

        public DataWrapper(int a_type, object a_data)
        {
            m_type = a_type;
            m_data = a_data;
        }

    }
}
