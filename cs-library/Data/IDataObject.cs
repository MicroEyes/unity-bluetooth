﻿using UnityEngine;
using System.Collections;
using com.microeyes.Serialization.Data;

namespace com.microeyes.Data
{
    public interface IDataObject 
    {
        void PutBool(string a_key, bool a_value);
        void PutByte(string a_key, byte a_value);
        void PutShort(string a_key, short a_value);
        void PutInt(string a_key, int a_value);
        void PutLong(string a_key, long a_value);
        void PutFloat(string a_key, float a_value);
        void PutDouble(string a_key, double a_value);
        void PutString(string a_key, string a_value);
        void PutBoolArray(string a_key, bool[] a_value);
        void PutByteArray(string a_key, byte[] a_value);
        void PutShortArray(string a_key, short[] a_value);
        void PutIntArray(string a_key, int[] a_value);
        void PutLongArray(string a_key, long[] a_value);
        void PutFloatArray(string a_key, float[] a_value);
        void PutDoubleArray(string a_key, double[] a_value);
        void PutStringArray(string a_key, string[] a_value);
        void PutDataObject(string a_key, IDataObject a_value);
        void PutDataArray(string a_key, DataObject[] a_value);


        bool GetBool(string a_key);
        byte GetByte(string a_key);
        short GetShort(string a_key);
        int GetInt(string a_key);
        long GetLong(string a_key);
        float GetFloat(string a_key);
        double GetDouble(string a_key);
        string GetString(string a_key);
        bool[] GetBoolArray(string a_key);
        byte[] GetByteArray(string a_key);
        short[] GetShortArray(string a_key);
        int[] GetIntArray(string a_key);
        long[] GetLongArray(string a_key);
        float[] GetFloatArray(string a_key);
        double[] GetDoubleArray(string a_key);
        string[] GetStringArray(string a_key);
        IDataObject GetDataObject(string a_key);
        DataWrapper GetData(string a_key);

        int Count { get; }
        bool Remove(string a_key);
        bool Contains(string a_key);
        string[] GetKeys();
        bool IsNull(string a_key);

        string Dump();
        ByteData ToByteData();

        object this[string a_key] { get; }
    }
}
