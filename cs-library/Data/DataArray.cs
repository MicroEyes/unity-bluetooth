﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace com.microeyes.Data
{
    public class DataArray  : IDataArray    {

        private List<DataWrapper> m_dataHolder = new List<DataWrapper>();

        public bool Contains(object a_value)
        {
            for (int l_index = 0; l_index < m_dataHolder.Count; l_index++)
            {
                if(object.Equals(GetElementAt(l_index), a_value))
                    return true;
            }

            return false;
        }

        public object GetElementAt(int a_index)
        {
            if (a_index >= m_dataHolder.Count)
                return null;
            else
                return m_dataHolder[a_index];
        }

        public object RemoveElementAt(int a_index)
        {
            if (a_index >= m_dataHolder.Count)
                return null;

            DataWrapper l_obj = m_dataHolder[a_index];
            m_dataHolder.RemoveAt(a_index);
            return l_obj.Data;
        }

        public int Count
        {
            get { return m_dataHolder.Count; }
        }

        void AddObject(object a_data, EDataType a_type)
        {
            m_dataHolder.Add(new DataWrapper((int)a_type, a_data));
        }

        T GetValue<T>(int a_index)
        {
            if (a_index >= m_dataHolder.Count)
                return default(T);
            else
                return (T)m_dataHolder[a_index].Data;
        }

        public void AddBool(bool a_value)
        {
            AddObject(a_value, EDataType.Bool);
        }

        public void AddByte(byte a_value)
        {
            AddObject(a_value, EDataType.Byte);
        }

        public void AddShort(short a_value)
        {
            AddObject(a_value, EDataType.Short);
        }

        public void AddInt(int a_value)
        {
            AddObject(a_value, EDataType.Int);
        }

        public void AddLong(long a_value)
        {
            AddObject(a_value, EDataType.Long);
        }

        public void AddFloat(float a_value)
        {
            AddObject(a_value, EDataType.Float);
        }

        public void AddDouble(double a_value)
        {
            AddObject(a_value, EDataType.Double);
        }

        public void AddString(string a_value)
        {
            AddObject(a_value, EDataType.String);
        }

        public void AddBoolArray(bool[] a_value)
        {
            AddObject(a_value, EDataType.Bool_Array);
        }

        public void AddByteArray(byte[] a_value)
        {
            AddObject(a_value, EDataType.Byte_Array);
        }

        public void AddShortArray(short[] a_value)
        {
            AddObject(a_value, EDataType.Short_Array);
        }

        public void AddIntArray(int[] a_value)
        {
            AddObject(a_value, EDataType.Int_Array);
        }

        public void AddLongArray(long[] a_value)
        {
            AddObject(a_value, EDataType.Long_Array);
        }

        public void AddFloatArray(float[] a_value)
        {
            AddObject(a_value, EDataType.Float_Array);
        }

        public void AddDoubleArray(double[] a_value)
        {
            AddObject(a_value, EDataType.Double_Array);
        }

        public void AddStringArray(string[] a_value)
        {
            AddObject(a_value, EDataType.String_Array);
        }

        public bool GetBool(int a_index)
        {
            return GetValue<bool>(a_index);
        }

        public byte GetByte(int a_index)
        {
            return GetValue<byte>(a_index);
        }

        public short GetShort(int a_index)
        {
            return GetValue<short>(a_index);
        }

        public int GetInt(int a_index)
        {
            return GetValue<int>(a_index);
        }

        public long GetLong(int a_index)
        {
            return GetValue<long>(a_index);
        }

        public float GetFloat(int a_index)
        {
            return GetValue<float>(a_index);
        }

        public double GetDouble(int a_index)
        {
            return GetValue<double>(a_index);
        }

        public string GetString(int a_index)
        {
            return GetValue<string>(a_index);
        }

        public bool[] GetBoolArray(int a_index)
        {
            return GetValue<bool[]>(a_index);
        }

        public byte[] GetByteArray(int a_index)
        {
            return GetValue<byte[]>(a_index);
        }

        public short[] GetShortArray(int a_index)
        {
            return GetValue<short[]>(a_index);
        }

        public int[] GetIntArray(int a_index)
        {
            return GetValue<int[]>(a_index);
        }

        public long[] GetLongArray(int a_index)
        {
            return GetValue<long[]>(a_index);
        }

        public float[] GetFloatArray(int a_index)
        {
            return GetValue<float[]>(a_index);
        }

        public double[] GetDoubleArray(int a_index)
        {
            return GetValue<double[]>(a_index);
        }

        public string[] GetStringArray(int a_index)
        {
            return GetValue<string[]>(a_index);
        }
    }
}
