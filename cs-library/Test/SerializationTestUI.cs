﻿using UnityEngine;
using System.Collections;
using com.microeyes.Serialization;
using com.microeyes.Serialization.Data;
using com.microeyes.Data;
using com.microeyes.bluetooth;


[ExecuteInEditMode]
public class SerializationTestUI : MonoBehaviour {

    //byte[] l_serializedData;
    IDataObject l_newObject;
    ByteData l_byteData;
    void OnGUI()
    {
        if(GUILayout.Button("Serialize"))
        {
            l_newObject = new DataObject();
            l_newObject.PutInt("1", 13);
            l_newObject.PutFloat("second", 11.4f);

            IDataObject l_child = new DataObject();
            l_child.PutString("thirds", "my_data");
            l_child.PutInt("fourth", 1333);

            l_newObject.PutDataObject("child", l_child);

            l_byteData = l_newObject.ToByteData();
            Debug.Log("First: " + l_byteData.Length);

            Debug.Log(l_newObject.Dump());
            Debug.Log(DataObject.PrettyPrint(l_newObject.Dump()));
        }

        if(GUILayout.Button("Deserialize"))
        {
            //DataObject l_obj = (DataObject)DefaultDataSerializer.ByteArrayToObject(l_byteData);
            //Debug.Log(l_obj.ToString());
        }

    }

	// Use this for initialization
	void Start () {

        
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
