﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using com.microeyes.bluetooth;
using System.Text;
using com.microeyes.Serialization;

[ExecuteInEditMode]
public class BlueConnectUI : MonoBehaviour
{

    [SerializeField]
    private string m_UUID = string.Empty;

    [SerializeField]
    private Rect m_rectInitialize = new Rect();

    //[SerializeField]
    //private Rect m_rectAttachEvents = new Rect();

    [SerializeField]
    private Rect m_rectCheckSupport = new Rect();

    [SerializeField]
    private Rect m_rectCheckEnabled = new Rect();

    [SerializeField]
    private Rect m_rectEnable = new Rect();
    
    [SerializeField]
    private Rect m_rectEnableDiscoverable = new Rect();

    [SerializeField]
    private Rect m_rectStartDiscovery = new Rect();

    [SerializeField]
    private Rect m_rectIsDiscovering = new Rect();

    [SerializeField]
    private Rect m_rectgetPairedDevices = new Rect();

    [SerializeField]
    private Rect m_rectShowDeviceList = new Rect();

    [SerializeField]
    private Rect m_rectStartListenAsServer = new Rect();

    [SerializeField]
    private Rect m_rectConnect = new Rect();

    [SerializeField]
    private Rect m_rectSendData = new Rect();

    [SerializeField]
    private Rect m_rectStop = new Rect();

    [SerializeField]
    private Rect m_rectLogs = new Rect();

    [SerializeField]
    private Rect m_rectBtnDeleteLog = new Rect();

    [SerializeField]
    private List<string> m_logs = new List<string>();

    [SerializeField]
    private Rect m_rectShowUpdate = new Rect();

    [SerializeField]
    private Rect m_rectDecrCount = new Rect();

    [SerializeField]
    private Rect m_rectIncrCount = new Rect();

    [SerializeField]
    private Rect m_rectDecrCountX = new Rect();

    [SerializeField]
    private Rect m_rectIncrCountX = new Rect();

    [SerializeField]
    private Rect m_rectLblCount = new Rect();

    string Logs
    {
        get 
        {
            string l_logs = string.Empty;

            foreach (string item in m_logs)
            {
                l_logs += ("\n" + item);
            }

            return l_logs;
        }
    }

    int m_count = 10;
    string m_str = "ssssssssss";

    void OnGUI()
    {
        if (GUI.Button(new Rect(m_rectInitialize.x * Screen.width, m_rectInitialize.y * Screen.height, m_rectInitialize.width * Screen.width, m_rectInitialize.height * Screen.height), "Initialize", "button"))
        {
            BluetoothManager.Initialize(m_UUID);
            AttachEvents();
        }

        if (GUI.Button(new Rect(m_rectCheckSupport.x * Screen.width, m_rectCheckSupport.y * Screen.height, m_rectCheckSupport.width * Screen.width, m_rectCheckSupport.height * Screen.height), "IsSupport", "button"))
        {
            AddLog("IsSupported: " + BluetoothManager.IsSupported);
        }

        if (GUI.Button(new Rect(m_rectCheckEnabled.x * Screen.width, m_rectCheckEnabled.y * Screen.height, m_rectCheckEnabled.width * Screen.width, m_rectCheckEnabled.height * Screen.height), "IsEnabled", "button"))
        {
            AddLog("l_isEnable: " + BluetoothManager.IsEnable);
        }

        if (GUI.Button(new Rect(m_rectEnable.x * Screen.width, m_rectEnable.y * Screen.height, m_rectEnable.width * Screen.width, m_rectEnable.height * Screen.height), "Enable", "button"))
        {
            BluetoothManager.Enable();
        }

        if (GUI.Button(new Rect(m_rectEnableDiscoverable.x * Screen.width, m_rectEnableDiscoverable.y * Screen.height, m_rectEnableDiscoverable.width * Screen.width, m_rectEnableDiscoverable.height * Screen.height), "Enable Discoverable", "button"))
        {
            BluetoothManager.EnableDiscoverable(BluetoothManager.DISCOVERABLE_TIME_MAX);
        }

        if (GUI.Button(new Rect(m_rectgetPairedDevices.x * Screen.width, m_rectgetPairedDevices.y * Screen.height, m_rectgetPairedDevices.width * Screen.width, m_rectgetPairedDevices.height * Screen.height), "Get Paired Devices", "button"))
        {
            List<BluetoothDevice> l_lstDevices = BluetoothManager.GetPairedDevices();
            AddLog("Paired Devices Count: " + l_lstDevices.Count);

            for (int l_index = 0; l_index < l_lstDevices.Count; l_index++)
            {
                AddLog(l_index + " ---- " + l_lstDevices[l_index].ToString());
            }

        }

        if (GUI.Button(new Rect(m_rectStartDiscovery.x * Screen.width, m_rectStartDiscovery.y * Screen.height, m_rectStartDiscovery.width * Screen.width, m_rectStartDiscovery.height * Screen.height), "Start Discovery", "button"))
        {
            BluetoothManager.StartDiscovery();
            AddLog("Started Discovery");
        }

        if (GUI.Button(new Rect(m_rectIsDiscovering.x * Screen.width, m_rectIsDiscovering.y * Screen.height, m_rectIsDiscovering.width * Screen.width, m_rectIsDiscovering.height * Screen.height), "IsDiscovering", "button"))
        {
            AddLog("IsDiscovering: " + BluetoothManager.IsDiscovering);
        }

        if (GUI.Button(new Rect(m_rectShowDeviceList.x * Screen.width, m_rectShowDeviceList.y * Screen.height, m_rectShowDeviceList.width * Screen.width, m_rectShowDeviceList.height * Screen.height), "DeviceList", "button"))
        {
            BluetoothManager.ShowDeviceList();
        }

        if (GUI.Button(new Rect(m_rectStartListenAsServer.x * Screen.width, m_rectStartListenAsServer.y * Screen.height, m_rectStartListenAsServer.width * Screen.width, m_rectStartListenAsServer.height * Screen.height), "Start Listen", "button"))
        {
            BluetoothManager.StartListening();
        }

        if (GUI.Button(new Rect(m_rectConnect.x * Screen.width, m_rectConnect.y * Screen.height, m_rectConnect.width * Screen.width, m_rectConnect.height * Screen.height), "Connect", "button"))
        {
            BluetoothManager.Connect(m_pickedDevice);
        }

        if (GUI.Button(new Rect(m_rectSendData.x * Screen.width, m_rectSendData.y * Screen.height, m_rectSendData.width * Screen.width, m_rectSendData.height * Screen.height), "Send Data", "button"))
        {
            StringBuilder l_str = new StringBuilder();

            for (int l_count = 0; l_count < m_count; l_count++)
            {
                l_str.Append(m_str);
            }

            BluetoothManager.SendText(l_str.ToString());
        }

        if (GUI.Button(new Rect(m_rectDecrCount.x * Screen.width, m_rectDecrCount.y * Screen.height, m_rectDecrCount.width * Screen.width, m_rectDecrCount.height * Screen.height), "-", "button"))
        {
            m_count -= 10;
        }

        if (GUI.Button(new Rect(m_rectDecrCountX.x * Screen.width, m_rectDecrCountX.y * Screen.height, m_rectDecrCountX.width * Screen.width, m_rectDecrCountX.height * Screen.height), "-", "button"))
        {
            m_count -= 100;
        }

        GUI.Label(new Rect(m_rectLblCount.x * Screen.width, m_rectLblCount.y * Screen.height, m_rectLblCount.width * Screen.width, m_rectLblCount.height * Screen.height), m_count.ToString(), "box");

        if (GUI.Button(new Rect(m_rectIncrCount.x * Screen.width, m_rectIncrCount.y * Screen.height, m_rectIncrCount.width * Screen.width, m_rectIncrCount.height * Screen.height), "+", "button"))
        {
            m_count += 10;
        }

        if (GUI.Button(new Rect(m_rectIncrCountX.x * Screen.width, m_rectIncrCountX.y * Screen.height, m_rectIncrCountX.width * Screen.width, m_rectIncrCountX.height * Screen.height), "+", "button"))
        {
            m_count += 100;
        }
        

        if (GUI.Button(new Rect(m_rectStop.x * Screen.width, m_rectStop.y * Screen.height, m_rectStop.width * Screen.width, m_rectStop.height * Screen.height), "Stop", "button"))
        {
            BluetoothManager.Stop();
        }

        if (GUI.Button(new Rect(m_rectShowUpdate.x * Screen.width, m_rectShowUpdate.y * Screen.height, m_rectShowUpdate.width * Screen.width, m_rectShowUpdate.height * Screen.height), "Show Update", "button"))
        {
            m_isShowUpdateLogs = !m_isShowUpdateLogs;
        }        

        GUI.Label(new Rect(m_rectLogs.x * Screen.width, m_rectLogs.y * Screen.height, m_rectLogs.width * Screen.width, m_rectLogs.height * Screen.height), Logs, "box");
        if (GUI.Button(new Rect(m_rectBtnDeleteLog.x * Screen.width, m_rectBtnDeleteLog.y * Screen.height, m_rectBtnDeleteLog.width * Screen.width, m_rectBtnDeleteLog.height * Screen.height), "-", "button"))
        {
            m_logs.RemoveAt(0);
        }
        
    }

    void AttachEvents()
    {
        BluetoothManager.OnEnabled += Callback_OnEnable;
        BluetoothManager.OnDisabled += Callback_OnDisabled;

        BluetoothManager.OnDiscoverableEnabled += Callback_OnDiscoveryEnabled;

        BluetoothManager.OnStateChanged += Callback_OnBtStateChanged;
        BluetoothManager.OnDiscoverableStateChanged += Callback_OnBtDiscoverableStateChanged;
        BluetoothManager.OnNewDeviceDiscovered += Callback_OnBtFoundNewDevice;
        BluetoothManager.OnDiscoveryStarted += Callback_OnDiscoveryStarted;
        BluetoothManager.OnDiscoveryFinished += Callback_OnDiscoveryFinished;
        BluetoothManager.OnSelectedDeviceToConnect += Callback_OnSelectedDeviceToConnect;
        BluetoothManager.OnConnectionStateChanged += Callback_OnConnectionStateChanged;
        BluetoothManager.OnConnectionFailed += Callback_OnConnectionFailed;
        BluetoothManager.OnConnectionLost += Callback_OnConnectionLost;
        BluetoothManager.OnConnectionEstablished += Callback_OnConnectionSuccess;

        BluetoothManager.OnTextMessageSent += Callback_OnTextMessageSent;
        BluetoothManager.OnTextMessageReceived += Callback_OnTextMessageReceived;
    }

    [SerializeField]
    private bool m_isShowUpdateLogs = false;

    [SerializeField]
    private BluetoothDevice m_pickedDevice = null;

    // Use this for initialization
    void Start()
    {
        //StartCoroutine("ClearLogs");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if(m_isShowUpdateLogs)
        {
            Debug.Log("Time: " + Time.time);
        }
    }

    void AddLog(string a_log)
    {
        m_logs.Add(a_log);
    }

    IEnumerator ClearLogs()
    {
        while(true)
        {
            yield return new WaitForSeconds(5.0f);
            if (m_logs.Count > 0)
            {
                m_logs.RemoveAt(0);
            }
        }
    }

    void Callback_OnEnable(bool a_isEnabled)
    {
        AddLog("Callback_OnEnabled: " + a_isEnabled);
    }

    void Callback_OnDisabled()
    {
        AddLog("Callback_OnDisabled");
    }

    void Callback_OnDiscoveryEnabled(bool a_isDiscoveryEnabled)
    {
        AddLog("Callback_OnDiscoveryEnabled: " + a_isDiscoveryEnabled);
    }

    void Callback_OnBtStateChanged(BluetoothManager.EBluetoothState a_newState)
    {
        AddLog("BluetoothUI::Callback_OnBtStateChanged: " + a_newState);
    }

    void Callback_OnBtDiscoverableStateChanged(BluetoothManager.EBluetoothDiscoverableState a_newState)
    {
        AddLog("BluetoothUI::Callback_OnBtDiscoverableStateChanged: " + a_newState);
    }

    void Callback_OnBtFoundNewDevice(BluetoothDevice l_newDevice)
    {
        AddLog("BluetoothUI::Callback_OnBtFoundNewDevice: ");
        AddLog(l_newDevice.ToString());
    }

    void Callback_OnDiscoveryStarted()
    {
        AddLog("BluetoothUI::Callback_OnDiscoveryStarted");
        AddLog("OnDiscoveryStarted");
    }

    void Callback_OnDiscoveryFinished()
    {
        AddLog("BluetoothUI::Callback_OnDiscoveryFinished");
        AddLog("OnDiscoveryFinished");
    }

    void Callback_OnSelectedDeviceToConnect(BluetoothDevice a_selectedDevice)
    {
        AddLog("BluetoothUI::Callback_OnSelectedDeviceToConnect: " + a_selectedDevice.ToString());
        m_pickedDevice = a_selectedDevice;
    }

    void Callback_OnConnectionStateChanged(BluetoothDevice a_device, BluetoothMessageHandler.EConnectionState a_connectionState)
    {
        AddLog("Callback_OnConnectionStateChanged: " + a_connectionState);
    }

    void Callback_OnConnectionFailed(BluetoothDevice a_device)
    {
        AddLog("Callback_OnConnectionFailed: " + (a_device == null ? "NULL" : a_device.ToString()));
    }

    void Callback_OnConnectionSuccess(BluetoothDevice a_device)
    {
        AddLog("Callback_OnConnectionSuccess: " + (a_device == null ? "NULL" : a_device.ToString()));
    }

    void Callback_OnConnectionLost(BluetoothDevice a_device)
    {
        AddLog("Callback_OnConnectionLost: " + (a_device == null ? "NULL" : a_device.ToString()));
    }

    void Callback_OnTextMessageSent(string a_msg)
    {
        AddLog("Callback_OnTextMessageSent: mes Sent: '" + a_msg + "'");
    }

    void Callback_OnTextMessageReceived(string a_msg)
    {
        AddLog("Callback_OnTextMessageReceived: mes received: '" + a_msg + "'");
    }

}