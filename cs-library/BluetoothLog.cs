﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.bluetooth
{
    public class BluetoothLog
    {

        public const int NONE = 1 << 0;    //0
        public const int DEBUG = 1 << 1;   //2
        public const int INFO = 1 << 2;    //4
        public const int WARNING = 1 << 3; //8
        public const int ERROR = 1 << 4;   //16

        public const int DEBUG_NONE = 0;       //0
        public const int DEBUG_LOW = 1;       //1
        public const int DEBUG_MEDIUM = 2;    //2
        public const int DEBUG_HIGH = 3;      //3

        public static bool IsLogTypeSet(int a_toCheck)
        {
            if ((BluetoothManager.LogType & a_toCheck) == a_toCheck)
                return true;
            else
                return false;
        }

    }
}