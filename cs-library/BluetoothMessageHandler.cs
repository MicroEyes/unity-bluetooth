﻿using UnityEngine;
using System;
using System.Collections;

namespace com.microeyes.bluetooth
{
    public class BluetoothMessageHandler
    {

        internal const string KEY_MESSAGE_TYPE = "message_type";
        internal const string KEY_TOAST_MSG_TYPE = "tmsg_type";
        internal const string KEY_MESSAGE = "message";
        internal const string KEY_CONNECTION_GONE_TYPE = "conn_gone_type";

        public const int TOAST_MSG_CONNECTION_FAILED = 0;
        public const int TOAST_MSG_CONNECTION_LOST = 1;

        public enum EMessageType
        {
            MESSAGE_STATE_CHANGE = 1,
            MESSAGE_READ = 2,
            MESSAGE_WRITE = 3,
            MESSAGE_DEVICE_NAME = 4,
            MESSAGE_TOAST = 5,
            MESSAGE_CONNECTION_GONE = 6
        }


        // Constants that indicate the current connection state
        public enum EConnectionState
        {
            STATE_NONE = 0,         // we're doing nothing
            STATE_LISTEN = 1,       // now listening for incoming connections
            STATE_CONNECTING = 2,   // now initiating an outgoing connection
            STATE_CONNECTED = 3     // now connected to a remote device
        }

        public static void OnReceivedMessage(string a_data)
        {
            var l_data = JSON.Parse(a_data);

            if (l_data[KEY_MESSAGE_TYPE] != null)
            {
                int l_messageType = l_data[KEY_MESSAGE_TYPE].AsInt;

                if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
                {
                    Debug.Log("BluetoothMessageHandler:: OnReceivedMessage: MessageType: " + l_messageType);
                }

                BluetoothDevice l_device = null;
                if (l_data[BluetoothDevice.KEY_DEVICE] != null)
                {
                    l_device = BluetoothManager.JSONToBluetoothDevice(l_data[BluetoothDevice.KEY_DEVICE]);
                }

                if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
                {
                    Debug.Log("BluetoothMessageHandler:: OnReceivedMessage:: Device: " + (l_device == null ? "NULL" : l_device.ToString()));
                }

                switch (l_messageType)
                {
                    case (int)EMessageType.MESSAGE_STATE_CHANGE:

                        if (l_data[BluetoothManager.KEY_NEW_STATE] != null)
                        {
                            int l_newState = l_data[BluetoothManager.KEY_NEW_STATE].AsInt;
                            Debug.Log("BluetoothMessageHandler:: OnReceivedMessage:: New State: " + l_newState);

                            if (BluetoothManager.OnConnectionStateChanged != null)
                            {
                                BluetoothManager.OnConnectionStateChanged(null, (EConnectionState)l_newState);
                            }
                            else
                            {
                                Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:: OnConnectionStateChanged:: No callback registered");
                            }


                            switch (l_newState)
                            {
                                case (int) EConnectionState.STATE_NONE:
                                    break;
                                case (int)EConnectionState.STATE_LISTEN:
                                    
                                    BluetoothManager.IsStartedListeningAsServer = true;

                                    if(BluetoothManager.OnStartListeningToIncomingConnections != null)
                                    {
                                        BluetoothManager.OnStartListeningToIncomingConnections.Invoke();
                                    }
                                    else
                                    {
                                        Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:::: STATE_LISTEN:: No callback registered");
                                    }
                                    break;
                                case (int)EConnectionState.STATE_CONNECTING:
                                    break;
                                case (int)EConnectionState.STATE_CONNECTED:
                                    
                                    if(BluetoothManager.IsStartedListeningAsServer)
                                    {
                                        Debug.Log("BluetoothMessageHandler::OnReceivedMessage:: This is SERVER");
                                        BluetoothManager.IsServer = true;
                                    }

                                    BluetoothManager.ShowToast("BluetoothMessageHandler::OnReceivedMessage::Connection Established. Server:" + BluetoothManager.IsServer);
                                    
                                    if(BluetoothManager.Logger != null)
                                    {
                                        BluetoothManager.Logger("BluetoothMessageHandler::OnReceivedMessage::Connection Established. Server:" + BluetoothManager.IsServer + ". Device: " + ((l_device!= null) ? l_device.ToString() : "null"));
                                    }

                                    if(BluetoothManager.OnConnectionEstablished != null)
                                    {
                                        BluetoothManager.OnConnectionEstablished.Invoke(l_device);
                                        BluetoothManager.RemoteDevice = l_device;
                                    }
                                    else
                                    {
                                        Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:::: STATE_CONNECTED:: No callback registered");
                                    }
                                    break;
                                default:
                                    break;
                            }

                        }
                        else
                        {
                            Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:: 'KEY_NEW_STATE' key not received");
                        }

                        break;

                    case (int)EMessageType.MESSAGE_WRITE:

                        if (l_data[KEY_MESSAGE] != null)
                        {
                            string l_textMessage = l_data[KEY_MESSAGE];
                            Debug.Log("BluetoothMessageHandler:: OnReceivedMessage:: WRITE_MESSAGE: " + l_textMessage);

                            if (BluetoothManager.OnTextMessageSent != null)
                            {
                                BluetoothManager.OnTextMessageSent.Invoke(l_textMessage);
                            }
                            else
                            {
                                Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:::: MESSAGE_WRITE:: No callback registered");
                            }
                            
                        }
                        else
                        {
                            Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage::MESSAGE_WRITE:: 'KEY_MESSAGE' key no received");
                        }

                        break;

                    case (int)EMessageType.MESSAGE_READ:
                        //if (l_data[KEY_MESSAGE] != null)
                        //{
                        //    string l_textMessage = l_data[KEY_MESSAGE];
                        //    Debug.Log("BluetoothMessageHandler:: OnReceivedMessage::MESSAGE_READ:: Msg:'" + l_textMessage + "'");

                        //    byte[] l_btyeArray = System.Text.Encoding.UTF8.GetBytes(l_textMessage);

                        //    BluetoothManager.OnReceived_Packet(l_btyeArray);

                        //    //if (BluetoothManager.OnTextMessageReceived != null)
                        //    //{
                        //    //    BluetoothManager.OnTextMessageReceived.Invoke(l_textMessage);
                        //    //}

                        //}
                        //else
                        //{
                        //    Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage::MESSAGE_READ:: 'KEY_MESSAGE' key no received");
                        //}
                        break;

                    case (int)EMessageType.MESSAGE_DEVICE_NAME:
                        break;

                    case (int)EMessageType.MESSAGE_TOAST:
                        break;

                    case (int) EMessageType.MESSAGE_CONNECTION_GONE:

                        BluetoothManager.RemoteDevice = null;
                        int l_toastMsgType = l_data[KEY_CONNECTION_GONE_TYPE].AsInt;
                        switch (l_toastMsgType)
                        {
                            case TOAST_MSG_CONNECTION_FAILED:
                                BluetoothManager.ShowToast("Connection Failed");

                                if (BluetoothManager.OnConnectionFailed != null)
                                {
                                    BluetoothManager.OnConnectionFailed.Invoke(l_device);
                                }
                                else
                                {
                                    Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:::: TOAST_MSG_CONNECTION_FAILED:: No callback registered");
                                }

                                break;

                            case TOAST_MSG_CONNECTION_LOST:
                                BluetoothManager.ShowToast("Connection Lost");

                                if (BluetoothManager.OnConnectionLost != null)
                                {
                                    BluetoothManager.OnConnectionLost.Invoke(l_device);
                                }
                                else
                                {
                                    Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:::: TOAST_MSG_CONNECTION_LOST:: No callback registered");
                                }

                                break;

                            default:
                                Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:: Unknown 'MESSAGE_TOAST' " + l_toastMsgType);
                                break;
                        }

                        break;

                    default:
                        Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:: Unknown 'MESSAGE_TYPE' " + l_messageType);
                        break;
                }

            }
            else
            {
                Debug.LogError("BluetoothMessageHandler:: OnReceivedMessage:: 'KEY_MESSAGE_TYPE' key not received");
            }
        }

        internal static void OnDataReceived(string a_data)
        {
            if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
            {
                Debug.Log("BluetoothMessageHandler:: OnReceivedMessage::MESSAGE_READ:: Msg:'" + a_data + "'");
            }

            byte[] l_byteArray = Convert.FromBase64String(a_data);

            if (BluetoothLog.IsLogTypeSet(BluetoothLog.DEBUG) && BluetoothManager.DebugLevel == BluetoothLog.DEBUG_HIGH)
            {
                Debug.Log("byte array Size: " + l_byteArray.Length);
            }

            BluetoothManager.OnReceived_Packet(l_byteArray);

            //byte[] l_btyeArray = System.Text.Encoding.UTF8.GetBytes(l_textMessage);
            //byte[] l_btyeArray = a_data.tob

            //BluetoothManager.OnReceived_Packet(l_btyeArray);
        }

    }
}
