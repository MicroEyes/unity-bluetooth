﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

namespace com.microeyes.Serialization.Data
{
    public class ByteData
    {

        private int m_position = 0;
        private byte[] m_dataBuffer;

        public int Position
        {
            get { return m_position; }
            set { m_position = value; }
        }

        public byte[] DataBuffer
        {
            get { return m_dataBuffer; }
            set { m_dataBuffer = value; }
        }

        public int Length
        {
            get { return m_dataBuffer.Length; }
        }

        public ByteData()
        {
            m_position = 0;
            m_dataBuffer = new byte[0];
        }

        public ByteData(byte[] a_data)
        {
            m_position = 0;
            m_dataBuffer = a_data;
        }

        public void WriteByte(byte a_data)
        {
            WriteBytes(new byte[1] { a_data });
        }

        public void WriteBytes(byte[] a_data)
        {
            WriteBytes(a_data, 0, a_data.Length);
        }

        public void WriteBytes(byte[] a_data, int a_startIndex, int a_length)
        {
            byte[] l_newBuffer = new byte[m_dataBuffer.Length + a_length];
            Buffer.BlockCopy(m_dataBuffer, 0, l_newBuffer, 0, m_dataBuffer.Length);
            Buffer.BlockCopy(a_data, a_startIndex, l_newBuffer, m_dataBuffer.Length, a_length);
            m_dataBuffer = l_newBuffer;
        }

        public void WriteBool(bool a_data)
        {
            WriteBytes(new byte[1] { (byte)(a_data ? 1 : 0) });
        }

        public void WriteShort(short a_data)
        {
            WriteBytes(BitConverter.GetBytes(a_data));
        }

        public void WriteUShort(ushort a_data)
        {
            WriteBytes(BitConverter.GetBytes(a_data));
        }

        public void WriteInt(int a_data)
        {
            WriteBytes(BitConverter.GetBytes(a_data));
        }

        public void WriteLong(long a_data)
        {
            WriteBytes(BitConverter.GetBytes(a_data));
        }

        public void WriteFloat(float a_data)
        {
            WriteBytes(BitConverter.GetBytes(a_data));
        }

        public void WriteDouble(double a_data)
        {
            WriteBytes(BitConverter.GetBytes(a_data));
        }

        public void WriteUTFString(string a_data)
        {
            int l_byteCount = GetUTFLength(a_data);
            if (l_byteCount > 32678)
            {
                throw new Exception("String length cannot be greater than 32768.");
            }

            WriteUShort(Convert.ToUInt16(l_byteCount));
            WriteBytes(Encoding.UTF8.GetBytes(a_data));
        }

        public byte ReadByte()
        {
            return m_dataBuffer[m_position++];
        }

        public byte[] ReadBytes(int a_length)
        {
            byte[] l_newByte = new byte[a_length];
            Buffer.BlockCopy(m_dataBuffer, m_position, l_newByte, 0, a_length);
            m_position += a_length;
            return l_newByte;
        }

        public bool ReadBool()
        {
            return (m_dataBuffer[m_position++] == 1);
        }

        public short ReadShort()
        {
            return BitConverter.ToInt16(ReadBytes(2), 0);
        }

        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(ReadBytes(2), 0);
        }

        public int ReadInt()
        {
            return BitConverter.ToInt32(ReadBytes(4), 0);
        }

        public long ReadLong()
        {
            return BitConverter.ToInt64(ReadBytes(8), 0);
        }

        public float ReadFloat()
        {
            return BitConverter.ToSingle(ReadBytes(4), 0);
        }

        public double ReadDouble()
        {
            return BitConverter.ToDouble(ReadBytes(8), 0);
        }

        public string ReadUTFString()
        {
            ushort l_length = ReadUShort();
            string l_str = Encoding.UTF8.GetString(m_dataBuffer, m_position, l_length);
            m_position += l_length;
            return l_str;
        }

        int GetUTFLength(string a_data)
        {
            int l_byteCount = 0;

            for (int l_charIndex = 0; l_charIndex < a_data.Length; l_charIndex++)
            {
                int l_charUTFCode = (int)a_data[l_charIndex];
                if (l_charUTFCode > 0 && l_charUTFCode <= byte.MaxValue)
                {
                    l_byteCount += 1;
                }
                else if (l_charUTFCode > 2047)
                {
                    l_byteCount += 3;
                }
                else
                {
                    l_byteCount += 2;
                }
            }

            return l_byteCount;
        }

    }
}
