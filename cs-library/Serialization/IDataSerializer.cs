﻿using UnityEngine;
using System.Collections;
using com.microeyes.Data;
using com.microeyes.Serialization.Data;

namespace com.microeyes.Serialization
{
    public interface IDataSerializer {

        ByteData ObjectToByteArray(IDataObject a_obj);

        IDataObject ByteArrayToObject(ByteData a_data);
	
}
}