﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System;
using System.IO.Compression;

namespace com.microeyes.Serialization
{
    public class SerializationUtil
    {
        public static byte[] SerializeToByteArray(object a_object)
        {
            if (a_object == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, a_object);

                return ms.ToArray();                
            }
        }

        public static T Deserialize<T>(byte[] a_rawBytes)
        {
            BinaryFormatter binForm = new BinaryFormatter();

            using (MemoryStream memStream = new MemoryStream())
            {
                memStream.Write(a_rawBytes, 0, a_rawBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                T obj = (T)binForm.Deserialize(memStream);
                return obj;
            }
            //return default(T);
        }

        public static string ByteArrayToString(byte[] a_rawByteArray)
        {
            return Encoding.UTF8.GetString(a_rawByteArray);
        }

        public static string ByteArrayToStringDirty(byte[] bytes, bool a_isFormatted)
        {
            StringBuilder sb;
            if (a_isFormatted)
                sb = new StringBuilder("new byte[] { ");
            else
                sb = new StringBuilder(string.Empty);

            foreach (var b in bytes)
            {
                if (a_isFormatted)
                    sb.Append(b + ", ");
                else
                    sb.Append(b);
            }
            if(a_isFormatted)
                sb.Append("}");

            return sb.ToString();
        }
        
       

    }

}
