﻿using UnityEngine;
using System;
using System.Collections;
using com.microeyes.Data;
using com.microeyes.Serialization.Data;

namespace com.microeyes.Serialization
{
    public class DefaultDataSerializer : IDataSerializer
    {

        static DefaultDataSerializer s_instance = null;
        public static DefaultDataSerializer Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new DefaultDataSerializer();
                }
                return s_instance;
            }
        }

        public ByteData ObjectToByteArray(IDataObject a_dataObject)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte(Convert.ToByte((int)EDataType.DataObject));  //Writing First DataObject
            l_newByteData.WriteShort(Convert.ToInt16(a_dataObject.Count));
            return Instance.ObjectToByteArray_Internal(a_dataObject, l_newByteData);
        }

        public IDataObject ByteArrayToObject(ByteData a_byteData)
        {
            if (a_byteData.Length < 3)
            {
                throw new Exception("Byte data size insufficient. Cannot decode. Size: " + a_byteData.Length + " byte(s).");
            }
            a_byteData.Position = 0;
            return Instance.ByteArrayToObject_Internal(a_byteData);
        }

        ByteData ObjectToByteArray_Internal(IDataObject a_data, ByteData a_byteData)
        {
            foreach (String l_key in a_data.GetKeys())
            {
                DataWrapper l_data = a_data.GetData(l_key);
                a_byteData = EncodeObjectKey(l_key, a_byteData);
                a_byteData = EncodeObject(l_data, a_byteData);
            }

            return a_byteData;
        }

        IDataObject ByteArrayToObject_Internal(ByteData a_byteData)
        {
            DataObject l_dataObject = new DataObject();
            EDataType l_checkFirstObj = (EDataType)a_byteData.ReadByte();
            if (l_checkFirstObj == EDataType.DataObject)
            {
                int l_size = a_byteData.ReadShort();
                if (l_size < 0)
                {
                    throw new Exception("Size is negative " + l_size + ". Cannot decode.");
                }

                try
                {
                    for (int l_objectIndex = 0; l_objectIndex < l_size; l_objectIndex++)
                    {
                        string l_key = a_byteData.ReadUTFString();
                        DataWrapper l_data = DecodeObject(a_byteData);
                        if (l_data == null)
                        {
                            throw new Exception("Couldn't decode value for key '" + l_key + "'");
                        }
                        else
                        {
                            l_dataObject.Put(l_key, l_data);
                        }

                    }

                }
                catch (Exception e)
                {
                    throw e;
                }

            }
            else
            {
                Debug.LogError("Check DefaultData Serializer");
            }


            return l_dataObject;
        }

        ByteData EncodeObject(DataWrapper a_data, ByteData a_byteData)
        {
            switch (a_data.EType)
            {
                case EDataType.Null:
                    a_byteData = Encode_Null(a_byteData);
                    break;
                case EDataType.Bool:
                    a_byteData = Encode_Bool(Convert.ToBoolean(a_data.Data), a_byteData);
                    break;
                case EDataType.Byte:
                    a_byteData = Encode_Byte(Convert.ToByte(a_data.Data), a_byteData);
                    break;
                case EDataType.Short:
                    a_byteData = Encode_Short(Convert.ToInt16(a_data.Data), a_byteData);
                    break;
                case EDataType.Int:
                    a_byteData = Encode_Int(Convert.ToInt32(a_data.Data), a_byteData);
                    break;
                case EDataType.Long:
                    a_byteData = Encode_Long(Convert.ToInt64(a_data.Data), a_byteData);
                    break;
                case EDataType.Float:
                    a_byteData = Encode_Float(Convert.ToSingle(a_data.Data), a_byteData);
                    break;
                case EDataType.Double:
                    a_byteData = Encode_Double(Convert.ToDouble(a_data.Data), a_byteData);
                    break;
                case EDataType.String:
                    a_byteData = Encode_UTFString(Convert.ToString(a_data.Data), a_byteData);
                    break;
                case EDataType.Bool_Array:
                    a_byteData = Encode_BoolArray((bool[])a_data.Data, a_byteData);
                    break;
                case EDataType.Byte_Array:
                    a_byteData = Encode_ByteArray((byte[])a_data.Data, a_byteData);
                    break;
                case EDataType.Short_Array:
                    a_byteData = Encode_ShortArray((short[])a_data.Data, a_byteData);
                    break;
                case EDataType.Int_Array:
                    a_byteData = Encode_IntArray((int[])a_data.Data, a_byteData);
                    break;
                case EDataType.Long_Array:
                    a_byteData = Encode_LongArray((long[])a_data.Data, a_byteData);
                    break;
                case EDataType.Float_Array:
                    a_byteData = Encode_FloatArray((float[])a_data.Data, a_byteData);
                    break;
                case EDataType.Double_Array:
                    a_byteData = Encode_DoubleArray((double[])a_data.Data, a_byteData);
                    break;
                case EDataType.String_Array:
                    a_byteData = Encode_StringArray((string[])a_data.Data, a_byteData);
                    break;
                case EDataType.DataObject:
                    a_byteData = AppendData(a_byteData, ObjectToByteArray((IDataObject)a_data.Data));
                    break;
                case EDataType.DataArray:
                    break;
                default:
                    break;
            }

            return a_byteData;
        }

        DataWrapper DecodeObject(ByteData a_byteData)
        {
            EDataType l_dataType = (EDataType)Convert.ToInt32(a_byteData.ReadByte());
            DataWrapper l_data = null;
            switch (l_dataType)
            {
                case EDataType.Null:
                    l_data = Decode_Null(a_byteData);
                    break;
                case EDataType.Bool:
                    l_data = Decode_Bool(a_byteData);
                    break;
                case EDataType.Byte:
                    l_data = Decode_Byte(a_byteData);
                    break;
                case EDataType.Short:
                    l_data = Decode_Short(a_byteData);
                    break;
                case EDataType.Int:
                    l_data = Decode_Int(a_byteData);
                    break;
                case EDataType.Long:
                    l_data = Decode_Long(a_byteData);
                    break;
                case EDataType.Float:
                    l_data = Decode_Float(a_byteData);
                    break;
                case EDataType.Double:
                    l_data = Decode_Double(a_byteData);
                    break;
                case EDataType.String:
                    l_data = Decode_String(a_byteData);
                    break;
                case EDataType.Bool_Array:
                    l_data = Decode_BoolArray(a_byteData);
                    break;
                case EDataType.Byte_Array:
                    l_data = Decode_ByteArray(a_byteData);
                    break;
                case EDataType.Short_Array:
                    l_data = Decode_ShortArray(a_byteData);
                    break;
                case EDataType.Int_Array:
                    l_data = Decode_IntArray(a_byteData);
                    break;
                case EDataType.Long_Array:
                    l_data = Decode_LongArray(a_byteData);
                    break;
                case EDataType.Float_Array:
                    l_data = Decode_FloatArray(a_byteData);
                    break;
                case EDataType.Double_Array:
                    l_data = Decode_DoubleArray(a_byteData);
                    break;
                case EDataType.String_Array:
                    l_data = Decode_StringArray(a_byteData);
                    break;
                case EDataType.DataObject:
                    a_byteData.Position--;
                    IDataObject l_obj = DecodeDataObject(a_byteData);
                    l_data = new DataWrapper(EDataType.DataObject, l_obj);
                    break;
                case EDataType.DataArray:
                    break;
                default:
                    break;
            }

            return l_data;
        }
        ByteData EncodeObjectKey(string a_key, ByteData a_byteData)
        {
            a_byteData.WriteUTFString(a_key);
            return a_byteData;
        }
        ByteData AppendData(ByteData a_destination, ByteData a_oldData)
        {
            a_destination.WriteBytes(a_oldData.DataBuffer);
            return a_destination;
        }

        #region Encoding

        ByteData Encode_Null(ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)0);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_Byte(byte a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Byte);
            l_newByteData.WriteByte(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_Bool(bool a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Bool);
            l_newByteData.WriteBool(a_data);
            return AppendData(a_byteData, l_newByteData);
        }

        ByteData Encode_Short(short a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Short);
            l_newByteData.WriteShort(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_Int(int a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Int);
            l_newByteData.WriteInt(a_data);
            return AppendData(a_byteData, l_newByteData);
        }

        ByteData Encode_Long(long a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Long);
            l_newByteData.WriteLong(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_Float(float a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Float);
            l_newByteData.WriteFloat(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_Double(double a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Double);
            l_newByteData.WriteDouble(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_UTFString(string a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.String);
            l_newByteData.WriteUTFString(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_ByteArray(byte[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Byte_Array);
            l_newByteData.WriteBytes(a_data);
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_BoolArray(bool[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Bool_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (bool l_boolValue in a_data)
            {
                l_newByteData.WriteBool(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_ShortArray(short[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Short_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (short l_boolValue in a_data)
            {
                l_newByteData.WriteShort(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_IntArray(int[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Int_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (int l_boolValue in a_data)
            {
                l_newByteData.WriteInt(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_LongArray(long[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Long_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (long l_boolValue in a_data)
            {
                l_newByteData.WriteLong(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_FloatArray(float[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Float_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (float l_boolValue in a_data)
            {
                l_newByteData.WriteFloat(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_DoubleArray(double[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.Double_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (double l_boolValue in a_data)
            {
                l_newByteData.WriteDouble(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }
        ByteData Encode_StringArray(string[] a_data, ByteData a_byteData)
        {
            ByteData l_newByteData = new ByteData();
            l_newByteData.WriteByte((byte)EDataType.String_Array);
            l_newByteData.WriteShort(Convert.ToInt16(a_data.Length));
            foreach (string l_boolValue in a_data)
            {
                l_newByteData.WriteUTFString(l_boolValue);
            }
            return AppendData(a_byteData, l_newByteData);
        }

        #endregion


        #region Decoding

        DataWrapper Decode_Null(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Null, null);
        }

        DataWrapper Decode_Byte(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Byte, a_byteData.ReadByte());
        }
        DataWrapper Decode_Bool(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Bool, a_byteData.ReadBool());
        }
        DataWrapper Decode_Short(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Short, a_byteData.ReadShort());
        }
        DataWrapper Decode_Int(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Int, a_byteData.ReadInt());
        }
        DataWrapper Decode_Long(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Long, a_byteData.ReadLong());
        }
        DataWrapper Decode_Float(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Float, a_byteData.ReadFloat());
        }
        DataWrapper Decode_Double(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.Double, a_byteData.ReadDouble());
        }
        DataWrapper Decode_String(ByteData a_byteData)
        {
            return new DataWrapper(EDataType.String, a_byteData.ReadUTFString());
        }

        DataWrapper Decode_ByteArray(ByteData a_byteData)
        {
            int l_length = a_byteData.ReadInt();
            if (l_length < 0)
                throw new Exception("Array negative size: " + (object)l_length);
            else
            {
                return new DataWrapper(EDataType.Byte_Array, (object)new ByteData(a_byteData.ReadBytes(l_length)));
            }
        }
        DataWrapper Decode_BoolArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            bool[] l_array = new bool[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadBool();
            }

            return new DataWrapper(EDataType.Bool_Array, l_array);
        }
        DataWrapper Decode_ShortArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            short[] l_array = new short[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadShort();
            }

            return new DataWrapper(EDataType.Short_Array, l_array);
        }
        DataWrapper Decode_IntArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            int[] l_array = new int[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadInt();
            }

            return new DataWrapper(EDataType.Int_Array, l_array);
        }
        DataWrapper Decode_LongArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            long[] l_array = new long[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadLong();
            }

            return new DataWrapper(EDataType.Long_Array, l_array);
        }
        DataWrapper Decode_FloatArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            float[] l_array = new float[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadFloat();
            }

            return new DataWrapper(EDataType.Float_Array, l_array);
        }
        DataWrapper Decode_DoubleArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            double[] l_array = new double[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadDouble();
            }

            return new DataWrapper(EDataType.Double_Array, l_array);
        }
        DataWrapper Decode_StringArray(ByteData a_byteData)
        {
            int l_length = CheckAndGetArrayLength(a_byteData);
            string[] l_array = new string[l_length];
            for (int l_index = 0; l_index < l_length; l_index++)
            {
                l_array[l_index] = a_byteData.ReadUTFString();
            }

            return new DataWrapper(EDataType.String_Array, l_array);
        }

        IDataObject DecodeDataObject(ByteData a_byteData)
        {
            DataObject l_obj = new DataObject();
            int l_type = a_byteData.ReadByte();
            if((EDataType)l_type == EDataType.DataObject)
            {
                int l_size = a_byteData.ReadShort();
                if(l_size < 0)
                {
                    throw new Exception("Size is negative: " + l_size);
                }

                try
                {
                    for (int l_index = 0; l_index < l_size; l_index++)
                    {
                        string l_key = a_byteData.ReadUTFString();
                        DataWrapper l_dataWrapper = DecodeObject(a_byteData);
                        if (l_dataWrapper == null)
                        {
                            throw new Exception("Couldn't decode key: " + l_key);
                        }

                        l_obj.Put(l_key, l_dataWrapper);                        
                    }
                }
                catch ( Exception e)
                {
                    throw e;
                }
            }
            else
            {
                throw new Exception("Expecting DataObject. Found Type: " + l_type);
            }

            return l_obj;
        }

        private int CheckAndGetArrayLength(ByteData a_byteData)
        {
            int l_length = a_byteData.ReadShort();
            if(l_length < 0)
            {
                throw new Exception("Array size is negative: " + l_length);
            }

            return l_length;
        }

        #endregion

    }
}
