package com.microeyes.bluetooth;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import Utilities.Constants;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.util.Log;

public class BluetoothManager {
	
	private static BluetoothAdapter s_bluetoothAdapter = null;
	public static UnityPlayerActivity s_unityPlayerActivity = null;
	
	public static void Initialize(final UnityPlayerActivity a_activity) {
		Log.d("ANDROIDsasadsad", "DoIT");
		s_unityPlayerActivity = a_activity;
		
		if(s_unityPlayerActivity == null)
		{
			Log.e(Constants.TAG_BLUETOOTH_MANAGER, "Unity player is NULL");
		}
		
		s_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		Log.i(Constants.TAG_BLUETOOTH_MANAGER, "Initialized OKAY");
	}
	
	public static boolean IsBluetoothSupported() {	
		return ((s_bluetoothAdapter == null) ? false : true);
	}
	
	public static boolean IsEnable() {
		return s_bluetoothAdapter.isEnabled();
	}
	
	public static void Enable() {
		
		s_unityPlayerActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent l_indent = new Intent(s_unityPlayerActivity, BluetoothEnableActivity.class);
				s_unityPlayerActivity.startActivity(l_indent);
				
			}
		});		
		
	}

}
