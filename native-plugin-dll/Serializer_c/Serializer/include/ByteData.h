#ifndef BYTEDATA_H_INCLUDED
#define BYTEDATA_H_INCLUDED

typedef unsigned char byte;

typedef struct ByteData
{
    int m_Length;
    byte *m_data;
}ByteData;

void Init(ByteData *byteData);
int getLen_Int(int a_int);
void WriteBytes_Internal(ByteData *byteData, const byte *a_byte, int a_size);

void WriteChar(ByteData *byteData, const char a_char);
void WriteByte(ByteData *byteData, const byte a_byte);
void WriteInt(ByteData *byteData, const int a_int);
void printData(const ByteData *byteData);


#endif // BYTEDATA_H_INCLUDED
