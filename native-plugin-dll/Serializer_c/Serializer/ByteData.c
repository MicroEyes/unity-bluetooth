#include"include/ByteData.h"
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void Init(ByteData *a_data)
{
    a_data->m_data = (byte*) malloc(1);
    a_data->m_Length = 0;
}

void WriteChar(ByteData *byteData, const char a_char)
{
    char l_arry[1];
    l_arry[0] = a_char;
    byte *a_byte = (byte*)(l_arry);
    WriteBytes_Internal(byteData, a_byte, 1);
}

void WriteByte(ByteData *a_byteData, const byte a_byte)
{
    /*printf("\nOldData: '%s'", a_byteData->m_data);
    char *buffer = (char*)malloc(getLen_Int(a_int));
    itoa(a_int, buffer, 10);
    printf("\nbuffer: %s", buffer);
    byte *l_data = (byte*)(buffer);
    printf("\nbuffer after: %s", buffer);
    WriteBytes_Internal(a_byteData, l_data, getLen_Int(a_int));*/

    /*byte l_arry[1];
    l_arry[0] = a_byte;
    WriteBytes_Internal(a_byteData, l_arry, 1);*/
}

void WriteInt(ByteData *a_byteData, const int a_int)
{
    printf("\nOldData: '%s'", a_byteData->m_data);
    char *buffer = (char*)malloc(getLen_Int(a_int));
    itoa(a_int, buffer, 10);
    printf("\nbuffer: %s", buffer);
    byte *l_data = (byte*)(buffer);
    printf("\nbuffer after: %s", buffer);
    WriteBytes_Internal(a_byteData, l_data, getLen_Int(a_int));
}

void WriteBytes_Internal(ByteData *byteData, const byte *a_bytes, int a_size)
{
    printf("\nBytes: %s, Len:: %d, \tOldData: '%s'", a_bytes, a_size, byteData->m_data);

    memcpy(byteData->m_data + byteData->m_Length, a_bytes, a_size);
    byteData->m_Length += a_size;
    printf("\nBytes After: '%s', \tLen: %d", byteData->m_data, byteData->m_Length);
}

int getLen_Int(int a_int)
{
    int l_len = 0;

    do
    {
        a_int /= 10;
        l_len++;
    }while(a_int);

    return l_len;
}

void printData(const ByteData *byteData)
{
    byte l_byteArray[byteData->m_Length + 1];

    int l_index = 0;
    while(l_index < byteData->m_Length)
    {
        l_byteArray[l_index] = byteData->m_data[l_index];
        l_index++;
    }

    l_byteArray[byteData->m_Length] = '\0';

    printf("\nData: '%s', Size: %d", l_byteArray, sizeof(l_byteArray));
}
