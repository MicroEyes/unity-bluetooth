#ifndef BYTEDATA_H_INCLUDED
#define BYTEDATA_H_INCLUDED

typedef unsigned char byte;
class ByteData
{
private:
    int m_position;
    int m_size;
    byte *m_data;
public:
    ByteData();
    int GetLength();
    int GetByteLength(const byte *a_data);
    void WriteByte(byte a_data);
    void WriteBytes(byte a_data[]);
    void WriteBytes_Internal(byte a_data[], int a_size);
    void WriteInt(int a_data);
    void Print();
    void PrintBytes(byte a_data[]);
    int getSize(byte *data, bool a_isIncludeNULLChar);
    void CharPToByte(char *a_p, byte *a_returnByte);
    //void CopyBytes(byte *a_src, byte *a_dest);
};


#endif // BYTEDATA_H_INCLUDED
