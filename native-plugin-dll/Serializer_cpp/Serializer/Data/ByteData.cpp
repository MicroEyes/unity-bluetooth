#include"include/ByteData.h"
#include <string.h>
#include <iostream>
#include <stdlib.h>

ByteData::ByteData()
{
    m_position = 0;
    m_size = 0;
}

int ByteData::GetLength()
{
    return 0;
}

int ByteData::GetByteLength(const byte *a_data)
{
    int l_length = 0;

    while(a_data[l_length] != '\0')
    {
        l_length++;
    }

    return l_length;
}

void ByteData::WriteByte(byte a_data)
{
    byte l_data[1] = {a_data};
    WriteBytes_Internal(l_data, 1);
}

void ByteData::WriteBytes(byte a_data[])
{
    std::cout<<"\nWriteBytes("<<sizeof(a_data)<<")";
    WriteBytes_Internal(a_data, sizeof(a_data));
}

void ByteData::WriteBytes_Internal(byte a_data[], int a_size)
{
    byte* l_newBuffer = NULL;
    if(m_size == 0) {
        l_newBuffer = new byte(a_size);
    } else {
        l_newBuffer = new byte(m_size + a_size);
        memcpy(l_newBuffer, m_data, m_size);
    }

    memcpy(l_newBuffer + m_size, a_data, a_size);
    m_size += a_size;
    delete m_data;
    m_data = NULL;
    m_data = l_newBuffer;
}

void ByteData::WriteInt(int a_data)
{
    char *p = new char();
    itoa(a_data, p, 10);
    std::cout<<"\n*p'"<<*p<<"', p:'"<<p<<"', &p'"<<&p<<"'";

    byte *a_byteData = new byte();
    //a_byteData = (byte*) &p;
    memcpy(a_byteData, p, strlen(p));
    //int l_length = GetByteLength(a_byteData);
    std::cout<<"\nFirst*a_byteData'"<<*a_byteData<<"', a_byteData:'"<<a_byteData<<"', &a_byteData'"<<&a_byteData<<"'";

    int l_length = 0;

    while(a_byteData[l_length] != '\0')
    {
        l_length++;
    }



    std::cout<<"\nLength: "<<l_length;
    //std::cout<<"\nLen: "<<strlen(*a_byteData);
    //CharPToByte(p, a_byteData);

    //byte b[] = static_cast<byte[]>(p);
    //WriteBytes_Internal(p, sizeof(p));

    std::cout<<"\n*a_byteData'"<<*a_byteData<<"', a_byteData:'"<<a_byteData<<"', &a_byteData'"<<&a_byteData<<"'";
    //std::cout<<"\nChar111("<<sizeof(p)<<"), '"<<p<<"'....Data:'"<<a_byteData<<"'";
}

int ByteData::getSize(byte *a_data, bool a_isIncludeNULLChar)
{
    char *q = reinterpret_cast<char *>(a_data);
    int l_size = strlen(q);
    delete q;
    q = NULL;
    return a_isIncludeNULLChar ? l_size : (l_size - 1);
}

void ByteData::PrintBytes(byte a_data[])
{
    std::cout<<"\nData: ---"<<a_data<<"----";
}

void ByteData::Print()
{
    std::cout<<"\nByteData("<<m_size<<"): '"<<m_data<<"'";
}

void ByteData::CharPToByte(char *a_p, byte *a_returnByte)
{
    std::cout<<"\n*a_p'"<<*a_p<<"', a_p:'"<<a_p<<"', &a_p'"<<&a_p<<"'";
    int l_length = strlen(a_p);

    int l_count = 0;

    byte l_data[l_length];
    while(l_count < l_length) {
        l_data[l_count] = a_p[l_count];
        l_count++;
    }

    std::cout<<"\n*l_data'"<<*l_data<<"', l_data:'"<<l_data<<"', &l_data'"<<&l_data<<"'";
    a_returnByte = l_data;
    std::cout<<"\n*a_returnByte'"<<*a_returnByte<<"', a_returnByte:'"<<a_returnByte<<"', &a_returnByte'"<<&a_returnByte<<"'";
}
