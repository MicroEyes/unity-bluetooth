#include<stdio.h>
#include<iostream>
#include"Serializer/Data/include/ByteData.h"

void DoIt(byte *p)
{
    std::cout<<"\nBefore *p:'"<<*p<<"', &p:'"<<&p<<"', p:'"<<p<<"'";

    char q[5] = {'a', 's', 'z', 'r'};

    *p = *q;

    std::cout<<"\nAfter *p:'"<<*p<<"', &p:'"<<&p<<"', p:'"<<p<<"'";
}

int main()
{
    //byte a = 'T';
    /*byte q[5] = {'q', 'w', 'e', 'r', 't' };
    std::cout<<"\nBefore *q:'"<<*q<<"', &q:'"<<&q<<"', q:'"<<q<<"'";
    DoIt(q);
    std::cout<<"\nAfter *q:'"<<*q<<"', &q:'"<<&q<<"', q:'"<<q<<"'";*/

    byte l_data;
    l_data = 'q';
    ByteData l_byteData;
    l_byteData.WriteByte(l_data);

    l_byteData.WriteInt(-1082899);

    /*byte l_data2;
    l_data2 = 'Y';
    l_byteData.WriteByte(l_data2);

    byte l_data3;
    l_data3 = 'a';
    l_byteData.WriteByte(l_data3);

    byte l_data4;
    l_data4 = 'L';
    l_byteData.WriteByte(l_data4);

    byte l_data5[5] = {'g', 'T', 'f', 'M', 'P'};
    //byte l_data5[3] = {'g', 'T', 'f'};
    std::cout<<"\nWWWriteBytes("<<sizeof(l_data5)<<")";
    l_byteData.WriteBytes_Internal(l_data5, sizeof(l_data5));
*/
    //l_byteData.Print();

    getchar();
    return 0;
}
