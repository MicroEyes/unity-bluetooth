package com.microeyes.bluetooth;

import com.microeyes.bluetooth.utilities.Constants;
import com.unity3d.player.UnityPlayer;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
//import android.view.WindowManager;

public class BluetoothInteractActivity extends Activity  {

	private boolean m_isRequestHandled = false;
	UnityPlayer m_unityPlayer = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		
		Window window = this.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		setContentView(R.layout.invisible_layout);
		
		m_unityPlayer = new UnityPlayer(this);
		
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Empty activity started");
		

		m_isRequestHandled = false;
		int l_request_id = getIntent().getIntExtra(Constants.KEY_REQUEST_ID, 0);
		
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "OnCreate");
		
		switch(l_request_id)
		{
			case Constants.REQUEST_BT_ENABLE: 
				if(!BluetoothManager.s_bluetoothAdapter.isEnabled()) {
					Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Sending Enable request");
					
					Intent l_btIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(l_btIntent, l_request_id);
				}
				break;
				
			case Constants.REQUEST_BT_DISCOVER:
				Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Sending Discoverable request");
				
				int l_discoverableTime = getIntent().getIntExtra(Constants.KEY_DISCOVERY_DURATION, Constants.DISCOVERABLE_TIME_DEFAULT);
				
				Intent l_btIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);				
				l_btIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, l_discoverableTime);
				startActivityForResult(l_btIntent, l_request_id);
				break;		
		}
		
	}
	
	@Override
	protected void onActivityResult(int a_requestCode, int a_resultCode, Intent a_data) {
		// TODO Auto-generated method stub
		super.onActivityResult(a_requestCode, a_resultCode, a_data);
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "OnActivityResult");
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "RequestCode: " + a_requestCode + ", ResultCode: " + a_resultCode);
		
		switch(a_requestCode)
		{
			case Constants.REQUEST_BT_ENABLE:
				OnReceived_EnableResponse(a_resultCode, a_data);
				break;
				
			case Constants.REQUEST_BT_DISCOVER:
				OnReceived_DiscoverableResponse(a_resultCode, a_data);
				break;
				
			default:
				break;
		}
	}
	
	void OnReceived_EnableResponse(int a_resultCode, Intent a_data) {
		m_isRequestHandled = true;
		
		if(a_resultCode != Activity.RESULT_CANCELED) {
			Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Enabled");
			BluetoothManager.OnBluetoothEnableRequest_OK();
		}
		else {
			Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Declined Enable");
			BluetoothManager.OnBluetoothEnableRequest_CANCELLED();
		}
		
		BluetoothManager.OnBluetoothEnableRespose((a_resultCode != Activity.RESULT_CANCELED));		
	}
	
	void OnReceived_DiscoverableResponse(int a_resultCode, Intent a_intent) {
		m_isRequestHandled = true;
		
		if(a_resultCode != Activity.RESULT_CANCELED) {
			Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Discoverable Enabled");
			BluetoothManager.OnBluetoothDiscoverableEnableRequest_OK();
		}
		else {
			Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "Declined Discoverable Enable");
			BluetoothManager.OnBluetoothDiscoverableEnableRequest_CANCELLED();
		}
		
		BluetoothManager.OnBluetoothDiscoverableEnableResponse((a_resultCode != Activity.RESULT_CANCELED));
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "OnResume: " + m_isRequestHandled);
		if(m_isRequestHandled) {
			finish();
		}
		
		m_unityPlayer.resume();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "onConfigurationChanged");
		m_unityPlayer.configurationChanged(newConfig);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "onWindowFocusChanged");
		m_unityPlayer.windowFocusChanged(hasFocus);
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		Log.i(Constants.TAG_BLUETOOTH_INTERACT_ACTIVITY, "dispatchKeyEvent");
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return m_unityPlayer.onKeyMultiple(event.getKeyCode(), event.getRepeatCount(), event);
		
		return super.dispatchKeyEvent(event);
	}
	
}
