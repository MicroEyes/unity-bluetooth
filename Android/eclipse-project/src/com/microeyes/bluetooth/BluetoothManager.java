package com.microeyes.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.microeyes.bluetooth.data.BluetoothMessageType;
import com.microeyes.bluetooth.data.DataPacket;
import com.microeyes.bluetooth.utilities.BTLog;
import com.microeyes.bluetooth.utilities.Constants;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerNativeActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Debug;
import android.util.Base64;
//import android.util.Log;
import android.widget.Toast;

public class BluetoothManager {
	
	public static BluetoothAdapter s_bluetoothAdapter = null;
	public static UnityPlayerNativeActivity s_unityPlayerActivity = null;   //Used UnityPlayerNativeActivity instead of UnityPlayerActivity, because unity default pass its reference as 'UnityPlayerNativeActivity'. 
	public static String UUID = "";

	
	private static BroadcastReceiver s_brEnableStateChanged = null;
	private static BroadcastReceiver s_brDiscoverableStateChanged = null;
	private static BroadcastReceiver s_brScanDeviceChanged = null;
	
	/*public static Set<BluetoothDevice> s_remoteDeviceListAll = new HashSet<BluetoothDevice>();
	public static Set<BluetoothDevice> s_remoteDeviceListPaired = new HashSet<BluetoothDevice>();
	public static Set<BluetoothDevice> s_remoteDeviceListUnPaired = new HashSet<BluetoothDevice>();*/
	
	public static Map<String, BluetoothDevice> s_remoteDeviceListAll = new HashMap<String, BluetoothDevice>();
	public static Map<String, BluetoothDevice> s_remoteDeviceListPaired = new HashMap<String, BluetoothDevice>();
	public static Map<String, BluetoothDevice> s_remoteDeviceListUnPaired = new HashMap<String, BluetoothDevice>();
	
	private static List<DataPacket> s_dataList = new ArrayList<DataPacket>();
	
	public static void Initialize(final UnityPlayerNativeActivity a_activity, String a_uuid, int a_logType, int a_debugLevel) {
		
		BTLog.LogType = a_logType;
		BTLog.DebugLevel = a_debugLevel;
		
		BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Initializing Bluetooth plugin");
		s_unityPlayerActivity = a_activity;
		
		if(s_unityPlayerActivity == null)
		{
			BTLog.Error(Constants.TAG_BLUETOOTH_MANAGER, "Unity player is NULL");
		}
		
		UUID = a_uuid;
		s_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		JsonObject l_data = new JsonObject();
		
		if(s_bluetoothAdapter != null) {
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Registering Boardcast receiver");

			s_brEnableStateChanged = new BluetoothStateChangeBoardcastReceiver();
			s_brDiscoverableStateChanged = new BluetoothDiscoverableStateChangeBroadcastReceiver();
			s_brScanDeviceChanged = new BluetoothScanDeviceBoardcastReceiver();
			
			
			s_unityPlayerActivity.registerReceiver(s_brEnableStateChanged,
					  new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
			
			s_unityPlayerActivity.registerReceiver(s_brDiscoverableStateChanged, 
					new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
			
			IntentFilter l_filter = new IntentFilter();
			l_filter.addAction(BluetoothDevice.ACTION_FOUND);
			l_filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
			l_filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
			
			s_unityPlayerActivity.registerReceiver(s_brScanDeviceChanged,
					l_filter);
		}

		l_data.add(Constants.KEY_IS_SUPPORTED, IsBluetoothSupported());
		if(IsBluetoothSupported()) {
			l_data.add(Constants.KEY_IS_ENABLED, IsEnable());
			l_data.add(Constants.KEY_IS_DISCOVERABLE, IsDiscoverable());
			l_data.add(Constants.KEY_DEVICE, MySelf_Internal());
			
			RegisterBondedDevices();
		}
		
		SendMessageToUnity(Constants.Event_OnInitialized, l_data.toString());
		BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Bluetooth plugin Initialized");
	}
	
	public static void Destroy() {
		if(s_bluetoothAdapter != null) {
			BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "Start Destroying Data", BTLog.DEBUG_MEDIUM);
			s_unityPlayerActivity.unregisterReceiver(s_brEnableStateChanged);			
			s_unityPlayerActivity.unregisterReceiver(s_brDiscoverableStateChanged);
			s_unityPlayerActivity.unregisterReceiver(s_brScanDeviceChanged);
			
			BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "Destroying Data Finished", BTLog.DEBUG_MEDIUM);
		}
		else {
			BTLog.Error(Constants.TAG_BLUETOOTH_MANAGER, "OnDestroy:: BluetoothAdapter is null");
		}
			
	}
	
	private static JsonObject MySelf_Internal() {
		return BluetoothDeviceToJsonObject(s_bluetoothAdapter.getName(), 
				s_bluetoothAdapter.getAddress(), 
				(s_bluetoothAdapter.getState() == BluetoothDevice.BOND_BONDED));
		
	}
	
	public static String MySelf() {		
		if(s_bluetoothAdapter == null) {
			return "";
		}
		
		return MySelf_Internal().toString();
	}
	
	public static boolean IsBluetoothSupported() {	
		return ((s_bluetoothAdapter == null) ? false : true);
	}
	
	public static boolean IsEnable() {
		return (s_bluetoothAdapter == null) ? false : s_bluetoothAdapter.isEnabled();
	}
	
	public static boolean IsDiscoverable() {
		return (s_bluetoothAdapter == null) ? false : (s_bluetoothAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE);
	}
	
	public static void Enable() {	
		
		if(s_bluetoothAdapter.isEnabled()) {
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Already Enabled");
			BluetoothManager.OnBluetoothDiscoverableEnableResponse(true);
		}
		else {
			s_unityPlayerActivity.runOnUiThread(new Runnable() {			
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent l_indent = new Intent(s_unityPlayerActivity, BluetoothInteractActivity.class);
					l_indent.putExtra(Constants.KEY_REQUEST_ID, Constants.REQUEST_BT_ENABLE);
					s_unityPlayerActivity.startActivity(l_indent);				
				}
			});
		}		
	}
	
	public static void EnableDiscoverable(final int a_discoveryDuration) {
		
		if(s_bluetoothAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
		{
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Already in discoverable");
			BluetoothManager.OnBluetoothDiscoverableEnableResponse(true);
		}
		else { 
			s_unityPlayerActivity.runOnUiThread(new  Runnable() {			
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					int l_discoveryDuration = a_discoveryDuration;
					if(l_discoveryDuration < Constants.DISCOVERABLE_TIME_MIN) {
						l_discoveryDuration = Constants.DISCOVERABLE_TIME_DEFAULT; 
					}
					else if(l_discoveryDuration > Constants.DISCOVERABLE_TIME_MAX) {
						l_discoveryDuration = Constants.DISCOVERABLE_TIME_DEFAULT;
					}
					
					Intent l_intent = new Intent(s_unityPlayerActivity, BluetoothInteractActivity.class);
					l_intent.putExtra(Constants.KEY_REQUEST_ID, Constants.REQUEST_BT_DISCOVER);
					l_intent.putExtra(Constants.KEY_DISCOVERY_DURATION, l_discoveryDuration);
					s_unityPlayerActivity.startActivity(l_intent);
				}
			});
		}
	}
	
	public static boolean StartDiscovery() {
		boolean l_isStartintDiscovery = false;
		
		if(!s_bluetoothAdapter.isDiscovering()) {
			l_isStartintDiscovery = true;
			s_remoteDeviceListAll.clear();
			s_remoteDeviceListPaired.clear();
			s_remoteDeviceListUnPaired.clear();
			//s_remoteDeviceListAll = s_remoteDeviceListPaired = s_bluetoothAdapter.getBondedDevices();
			
			RegisterBondedDevices();
			
			s_bluetoothAdapter.startDiscovery();
		}
		else {
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Already discovering");
		}
		
		return l_isStartintDiscovery;
	}
	
	private static void RegisterBondedDevices() {
		Set<BluetoothDevice> l_setDevicesLst = s_bluetoothAdapter.getBondedDevices();
		
		for(BluetoothDevice l_device : l_setDevicesLst) {
			s_remoteDeviceListPaired.put(l_device.getAddress(), l_device);
			s_remoteDeviceListAll.put(l_device.getAddress(), l_device);
		}
	}
	
	public static boolean CancelDiscovery() {
		
		boolean l_isCancelled = false;
		
		if(s_bluetoothAdapter.isDiscovering()) {
			s_bluetoothAdapter.cancelDiscovery();
			l_isCancelled = true;
		}
		else {
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Not in discovery mode");
		}
		
		return l_isCancelled;
	}
	
	public static boolean IsDiscovering() {
		return s_bluetoothAdapter.isDiscovering();
	}
	
	public static void ShowDeviceList() {
		s_unityPlayerActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent l_indent = new Intent(s_unityPlayerActivity, DeviceListActivity.class);
				s_unityPlayerActivity.startActivity(l_indent);
			}
		});		
	}

	public static void OnBluetoothEnableRespose(boolean a_isEnabled) {
		JsonObject l_obj = new JsonObject();
		l_obj.add(Constants.KEY_IS_ENABLED,  a_isEnabled);
		
		BluetoothManager.SendMessageToUnity(Constants.Event_OnBluetoothEnabledByUser, l_obj.toString());
	}
	
	public static void OnBluetoothEnableRequest_OK() {
		//register BroadcastReceiver
		BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "OnBluetoothEnableRequest_OK");
	}
	
	public static void OnBluetoothEnableRequest_CANCELLED() {
		
	}
	
	public static void OnBluetoothDiscoverableEnableResponse(boolean a_isEnabled) {
		JsonObject l_obj = new JsonObject();
		l_obj.add(Constants.KEY_IS_ENABLED,  a_isEnabled);		
		
		BluetoothManager.SendMessageToUnity(Constants.Event_OnBluetoothDiscoverableEnabledByUser, l_obj.toString());
	}
	
	public static void OnBluetoothDiscoverableEnableRequest_OK() {
		
	}
	
	public static void OnBluetoothDiscoverableEnableRequest_CANCELLED() {
		
	}
	
	public static void OnBluetoothStateChanged(int a_newBTState) {
		JsonObject l_obj = new JsonObject();
		l_obj.add(Constants.KEY_NEW_STATE, a_newBTState);
		SendMessageToUnity(Constants.Event_OnBluetoothStateChanged, l_obj.toString());
	}
	
	public static void OnBluetoothDiscoverableStateChanged(int a_newDiscoverableState){
		JsonObject l_obj = new JsonObject();
		l_obj.add(Constants.KEY_NEW_STATE, a_newDiscoverableState);
		SendMessageToUnity(Constants.Event_OnBluetoothDiscoverableStateChanged, l_obj.toString());	
	}
	
	public static void OnBluetoothDiscoveryStarted() {
		SendMessageToUnity(Constants.Event_OnBluetoothDiscoverableStarted, "");
	}
	
	public static void OnBluetoothDiscoveryFinished() {
		SendMessageToUnity(Constants.Event_OnBluetoothDiscoverableFinished, "");
	}	
	
	/**
	 * Return list of Only Paired devices  
	 * @return
	 */
	public static String GetPairedDevices() {
		//Set<BluetoothDevice> l_pairedDevices = s_bluetoothAdapter.getBondedDevices();		
		String l_returningData = GetStringForDevices(s_remoteDeviceListPaired);
		return l_returningData;
	}
	
	/**
	 * Returns Only New Devices found in Discovery
	 * @return
	 */
	public static String GetNewDiscoveredDevices() {		
		String l_returningData = GetStringForDevices(s_remoteDeviceListUnPaired);
		return l_returningData;
	}
	
	/**
	 * Returns List of Paired devices + Newly discovered devices
	 * @return
	 */
	public static String GetDiscoveredDevices() {
		/*Set<BluetoothDevice> l_pairedDevices = s_bluetoothAdapter.getBondedDevices();
		l_pairedDevices.addAll(s_remoteDeviceListAll);*/
		String l_returningData = GetStringForDevices(s_remoteDeviceListAll);
		return l_returningData;
	}
	
	private static String GetStringForDevices(Map<String, BluetoothDevice> a_devicesList) {
		JsonObject l_obj = new JsonObject();
		JsonArray l_arry = new JsonArray();
		
		for(String l_deviceAddress : a_devicesList.keySet()) {
			JsonObject l_deviceJson = BluetoothDeviceToJsonObject(a_devicesList.get(l_deviceAddress));
			
			l_arry.add(l_deviceJson);
		}
		
		l_obj.add(Constants.KEY_DEVICE_DEVICE_LIST, l_arry);
		
		return l_obj.toString();
	}
	
	public static void OnFoundNewDevice(BluetoothDevice l_newDevice) {
		
		BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "OnFoundNewDevice:: New Device Found: " + l_newDevice.getName());
		// If it's already paired, skip it, because it's been listed already
		if(l_newDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "OnFoundNewDevice:: New Device Found not bonded: " + l_newDevice.getName());
			s_remoteDeviceListUnPaired.put(l_newDevice.getAddress(), l_newDevice);
			s_remoteDeviceListAll.put(l_newDevice.getAddress(), l_newDevice);
			
			if(DeviceListActivity.getInstance() != null) {
				DeviceListActivity.getInstance().OnDiscoveredNewDevice(l_newDevice);
			}
			
			JsonObject l_deviceJson = BluetoothDeviceToJsonObject(l_newDevice);
			//l_deviceJson.add(Constants.KEY_DEVICE_IS_PAIRED, false);
			
			SendMessageToUnity(Constants.Event_OnNewDeviceFound, l_deviceJson.toString());
		}
		else {
			BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Device found already paired: " + l_newDevice.getName());
		}
	}
	
	static JsonObject BluetoothDeviceToJsonObject(BluetoothDevice l_device) {
		JsonObject l_deviceJsonObj = new JsonObject();
		l_deviceJsonObj.add(Constants.KEY_DEVICE_NAME, l_device.getName());
		l_deviceJsonObj.add(Constants.KEY_DEVICE_ADDRESS, l_device.getAddress());
		l_deviceJsonObj.add(Constants.KEY_DEVICE_IS_PAIRED, (l_device.getBondState() == BluetoothDevice.BOND_BONDED));

		return l_deviceJsonObj;
	}
	
	static JsonObject BluetoothDeviceToJsonObject(String a_name, String a_address, boolean a_isPaired) {		
		JsonObject l_deviceJsonObj = new JsonObject();
		l_deviceJsonObj.add(Constants.KEY_DEVICE_NAME, a_name);
		l_deviceJsonObj.add(Constants.KEY_DEVICE_ADDRESS, a_address);
		l_deviceJsonObj.add(Constants.KEY_DEVICE_IS_PAIRED, a_isPaired);
		
		return l_deviceJsonObj;
	}
	
	public static void OnSelectedDeviceToConnect(String a_address) {
		BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "OnSelectedBluetoothDeviceToConnect:: Address: " + a_address, BTLog.DEBUG_MEDIUM);
		
		BluetoothDevice l_device = s_remoteDeviceListAll.get(a_address);
		JsonObject l_data = BluetoothDeviceToJsonObject(l_device);
		
		SendMessageToUnity(Constants.Event_OnSelectedDeviceToConnect, l_data.toString());
	}
	
	public static void StartListening() {
		BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "StartListening: ");
		s_unityPlayerActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "Connect::Before Start ", BTLog.DEBUG_HIGH);
					BluetoothConnectionHandler.getInstance().StartListening();
					BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "Connect::After resume ", BTLog.DEBUG_HIGH);
				}
				catch(Exception e) {
					BTLog.Error(Constants.TAG_BLUETOOTH_MANAGER, "Error: " + e.getMessage());
					e.printStackTrace();
				}	
			}
		});
	}
	
	private static BluetoothDevice s_connectingToDevice = null;
	public static void Connect(final String a_address) {
		BTLog.Info(Constants.TAG_BLUETOOTH_MANAGER, "Connect: device: " + a_address);
		
		s_unityPlayerActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				BluetoothDevice l_deviceToConnect = s_remoteDeviceListAll.get(a_address);
				s_connectingToDevice = l_deviceToConnect;
				BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "Connect: deviceName: " + l_deviceToConnect.getName(), BTLog.DEBUG_HIGH);
				BluetoothConnectionHandler.getInstance().Connect(l_deviceToConnect);
			}
		});
	}
	
	public static void SendPacket(final byte[] a_byteArray, final boolean a_isReflected) {
		s_unityPlayerActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				//OnDataReceived(a_byteArray);
				DataPacket l_dataPacket = new DataPacket(a_byteArray, a_isReflected);				
				BluetoothConnectionHandler.getInstance().SendPacket(l_dataPacket);
			}
		});
	}
	
	public static byte[] GetReflectedPacketData() {
		if(s_dataList.size() > 0) {
			DataPacket l_packet;
			synchronized (s_dataList) {
			 	l_packet = s_dataList.remove(0);				
			}
			return l_packet.getData();
		}
		return null;
	}
	
	public static void SetNewReflectedData(DataPacket a_dataPacket) {
		synchronized (s_dataList) {
			s_dataList.add(a_dataPacket);			
		}
	}
	
	public static void Stop() {
		BluetoothConnectionHandler.getInstance().stop();
	}
	
	public static void destroy() {
		BluetoothConnectionHandler.getInstance().destroy();
	}
	
	public static void OnDataReceived(byte[] a_data) {
		BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "OnDataReceived: " + a_data.length, BTLog.DEBUG_HIGH);
		
		String l_data = Base64.encodeToString(a_data, Base64.DEFAULT);
		BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "OnDataReceived:: Data: '" + l_data + "'", BTLog.DEBUG_HIGH);
		SendMessageToUnity(Constants.Event_OnDataReceived, l_data);
	}
	
	public static void OnMessageTypeReceived(JsonObject a_newMsgType) {
		BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "OnMessageTypeReceived:: NewMsgType: " + a_newMsgType, BTLog.DEBUG_HIGH);
				
		if(s_connectingToDevice != null) {
			BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "Adding Device: " + s_connectingToDevice.getName(), BTLog.DEBUG_HIGH);
			JsonObject l_obj = BluetoothDeviceToJsonObject(s_connectingToDevice);
			a_newMsgType.add(Constants.KEY_DEVICE, l_obj);
		}
		
		SendMessageToUnity(Constants.Event_OnMessageTypeReceived, a_newMsgType.toString());

		HandleMessage(a_newMsgType);
	}
	
	//TODO: set null for s_ConnectingToDevice on connection lost or failed
	static void HandleMessage(JsonObject l_obj) {
		int l_messageType = l_obj.getInt(Constants.KEY_MESSAGE_TYPE, -1);
		
		if(l_messageType == -1) {
			BTLog.Error(Constants.TAG_BLUETOOTH_MANAGER, "MessageType not found");
			return;
		}
		
		switch(l_messageType) {
			case BluetoothMessageType.MESSAGE_CONNECTION_GONE:
				s_connectingToDevice = null;
				break;
		}		
	}
	
	/*public static void OnConnectionStateChanged(int a_newState) {
		Log.i(Constants.TAG_BLUETOOTH_MANAGER, "OnConnectionStateChanged:: NewState: " + a_newState);
		
		JsonObject l_obj = new JsonObject();
		l_obj.add(Constants.KEY_NEW_STATE, a_newState);
		
		SendMessageToUnity(Constants.Event_OnConnectionStateChanged, l_obj.toString());
	}*/
	
	public static void SendMessageToUnity(String a_functionName, String a_data) {		
		BTLog.Debug(Constants.TAG_BLUETOOTH_MANAGER, "####### SendMessageToUnity: Function: " + a_functionName + ", Data: " + a_data, BTLog.DEBUG_HIGH);		
		UnityPlayer.UnitySendMessage(Constants.RECEIEVER_GAMEOBJECT, a_functionName, a_data);
	}
	
	public static void ShowToast(final String a_msg) {
		s_unityPlayerActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(s_unityPlayerActivity, a_msg,
		                Toast.LENGTH_SHORT).show();				
			}
		});
	}
	
}
