package com.microeyes.bluetooth.utilities;

import android.util.Log;


public class BTLog {
	
	public final static int NONE = 1 << 0;    //0
    public final static int DEBUG = 1 << 1;   //2
    public final static int INFO = 1 << 2;    //4
    public final static int WARNING = 1 << 3; //8
    public final static int ERROR = 1 << 4;   //16

    public final static int DEBUG_NONE = 0;       //0
    public final static int DEBUG_LOW = 1;       //1
    public final static int DEBUG_MEDIUM = 2;    //2
    public final static int DEBUG_HIGH = 3;      //4
	
	public static int LogType = 0;	
	public static int DebugLevel = 0;	
	
	public static void Debug (String a_tag, String a_log, int a_debugLevel) {
		if(((LogType & DEBUG) == DEBUG) && DebugLevel >= a_debugLevel) {
			Log.d(a_tag, a_log);
		}
	}
	
	public static void Info(String a_tag, String a_log) {
		if((LogType & INFO) == INFO) {
			Log.i(a_tag, a_log);
		}
	}
	
	public static void Warning(String a_tag, String a_log) {
		if((LogType & WARNING) == WARNING) {
			Log.w(a_tag, a_log);
		}
	}
	
	public static void Error(String a_tag, String a_log) {
		if((LogType & ERROR) == ERROR) {
			Log.e(a_tag, a_log);
		}
	}

}
