package com.microeyes.bluetooth.utilities;

public class Constants {

	public static final String RECEIEVER_GAMEOBJECT = "____BluetoothManager";
	
	public static final String TAG_BLUETOOTH_MANAGER = "microeyes_bt_mgr";
	public static final String TAG_BLUETOOTH_INTERACT_ACTIVITY = "microeyes_bt_interact_act";
	public static final String TAG_BLUETOOTH_STATE_CHANGE_RECEIVER = "microeyes_bt_stat_chng_receiver";
	public static final String TAG_BLUETOOTH_DISCVR_STATE_CHANGE_RECEIVER = "microeyes_bt_discvr_stat_chng_receiver";
	public static final String TAG_BLUETOOTH_BT_ACCEPT_THREAD = "microeyes_bt_accept_thread";

	public static final String TAG_BLUETOOTH_CONNECTION_HANDLER = "microeyes_bt_conn_handler";
	public static final String TAG_BLUETOOTH_CONNECTION_SERVICE = "microeyes_bt_conn_service";
		
	public static final int REQUEST_BT_ENABLE = 0;
	public static final int REQUEST_BT_DISCOVER = 1;	
	
	public static final String Event_OnInitialized = "OnInitialized";
	public static final String Event_OnBluetoothEnabledByUser = "OnEnabledByUser";
	public static final String Event_OnBluetoothDiscoverableEnabledByUser = "OnDiscoverableEnabledByUser";
	public static final String Event_OnBluetoothStateChanged = "OnStateChanged";
	public static final String Event_OnBluetoothDiscoverableStateChanged = "OnDiscoverableStateChanged";
	public static final String Event_OnBluetoothDiscoverableStarted = "OnDiscoveryStarted";
	public static final String Event_OnBluetoothDiscoverableFinished = "OnDiscoveryFinished";
	public static final String Event_OnNewDeviceFound = "OnNewDeviceDiscovered";
	public static final String Event_OnSelectedDeviceToConnect = "OnSelectedDeviceToConnect";
	public static final String Event_OnConnectionStateChanged = "OnConnectionStateChanged";
	public static final String Event_OnMessageTypeReceived = "OnMessageTypeReceived";
	public static final String Event_OnDataReceived = "OnDataReceived";
	
	public static final String KEY_REQUEST_ID = "requestId";
	
	public static final String KEY_DEVICE = "device";
	public static final String KEY_DEVICE_NAME = "name";
	public static final String KEY_DEVICE_ADDRESS = "address";
	public static final String KEY_DEVICE_IS_PAIRED = "isPaired";
	public static final String KEY_DEVICE_DEVICE_LIST = "lstDevice";
	
	public static final String KEY_DISCOVERY_DURATION = "discovery_duration";
	
	public static final String KEY_NEW_STATE = "newState";
	public static final String KEY_IS_ENABLED = "isEnabled";
	public static final String KEY_IS_SUPPORTED = "isSupported";
	public static final String KEY_IS_DISCOVERABLE = "isDiscoverable";
	
	public static final String KEY_MESSAGE_TYPE = "message_type";
	public static final String KEY_TOAST_MSG_TYPE = "tmsg_type";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_CONNECTION_GONE_TYPE = "conn_gone_type";
	
	public static final int MSG_CONNECTION_FAILED = 0;
	public static final int MSG_CONNECTION_LOST = 1;
	

	public static final int DISCOVERABLE_TIME_MIN = 0;
	public static final int DISCOVERABLE_TIME_MAX = 3600;
	public static final int DISCOVERABLE_TIME_DEFAULT = 120;
}
