package com.microeyes.bluetooth;

import com.eclipsesource.json.JsonObject;
import com.microeyes.bluetooth.data.BluetoothMessageType;
import com.microeyes.bluetooth.data.DataPacket;
import com.microeyes.bluetooth.utilities.BTLog;
import com.microeyes.bluetooth.utilities.Constants;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class BluetoothConnectionHandler {

	private static BluetoothConnectionHandler s_instance = null;
	public static BluetoothConnectionHandler getInstance() {
		if(s_instance == null) {
			Log.i("Unity", "Creating instance");
			s_instance = new BluetoothConnectionHandler();
		}		
		return s_instance;
	}
	
	BluetoothConnectionService m_service = null;
 	Context s_context = null;
 	private final boolean D = true;

 	//String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
 	
    /*public void Init() {
    	if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "--- INIT ---");
    	onStart();
    	onResume();
    }*/
    
    public void StartListening() {
    	if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "--- StartListening ---");

		if(m_service == null) {
			onStart();
		}
    	
    	if(m_service != null) {
	    	if (m_service.getState() == BluetoothConnectionService.STATE_NONE) {
	            // Start the Bluetooth chat services
	          	m_service.startListening();
	          }
	          else {
	          	Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "StartListening:: Service State: " + m_service.getState());
          }
    	}
    	else {
    		Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "StartListening:: Service cannot be initialized");
    	}
    }
	
	public void Connect(BluetoothDevice a_deviceToConnect) {
		if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "--- CONNECT ---" + a_deviceToConnect);
		
		if(m_service == null) {
			onStart();
		}
		
		if(m_service != null) {
			m_service.connect(a_deviceToConnect);
		}
    	else {
    		Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "Connect:: Service cannot be initialized");
    	}
	}
    
	public void onStart() {
		if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "+ ON START +");
		if(m_service == null) {
			if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "+ ON CREATING +");
			m_service = new BluetoothConnectionService(BluetoothManager.s_unityPlayerActivity, mHandler);
			s_context = BluetoothManager.s_unityPlayerActivity;
			
			mOutStringBuffer = new StringBuffer("");			
		}
		else {
			Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "OnStart:: Service already created");
		}
	}
	
	/*public void onResume() {
		if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "+ ON RESUME +");
		
		 // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (m_service != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (m_service.getState() == BluetoothConnectionService.STATE_NONE) {
              // Start the Bluetooth chat services
            	m_service.startListening();
            }
            else {
            	Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "OnResume:: Service State: " + m_service.getState());
            }
        }
		else {
			Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "OnStart:: Service is NULL");
		}
	}*/
	
	public void destroy() {
		if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "--- ON DESTROY ---");
		if (m_service != null) { 
			m_service.stop();
		}
	}
	
	public void stop() {
		if(D) Log.e(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "--- ON DESTROY ---");
		
		if(m_service != null) {
			m_service.stop();
		}
	}
	 
	 /**
     * Sends Raw byte.
     * @param DataPacket to send.
     */    
    public void SendPacket(DataPacket a_dataPacket) {
    	
    	BTLog.Debug(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "SendPacket::", BTLog.DEBUG_HIGH);
        // Check that we're actually connected before trying anything
        if (m_service.getState() != BluetoothConnectionService.STATE_CONNECTED) {
            //Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (a_dataPacket.getData().length > 0) {
        	BTLog.Debug(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "SendPacket of length: " + a_dataPacket.getData().length, BTLog.DEBUG_HIGH);
            
            //tell the BluetoothService to write
            m_service.write(a_dataPacket);
            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            /*mOutEditText.setText(mOutStringBuffer);*/
        }
    }
	
	// The Handler that gets information back from the BluetoothService
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	BTLog.Debug(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "Handler:: msg:" + msg.what, BTLog.DEBUG_HIGH);
        	JsonObject l_obj = new JsonObject();
    		l_obj.add(Constants.KEY_MESSAGE_TYPE, msg.what);
    		
            switch (msg.what) {
            case BluetoothMessageType.MESSAGE_STATE_CHANGE:
                if(D) Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                l_obj.add(Constants.KEY_NEW_STATE, msg.arg1);
                
                switch (msg.arg1) {
	                case BluetoothConnectionService.STATE_CONNECTED:
	                    /*mTitle.setText(R.string.title_connected_to);
	                    mTitle.append(mConnectedDeviceName);
	                    mConversationArrayAdapter.clear();*/
	                    break;
	                case BluetoothConnectionService.STATE_CONNECTING:
	                    /*mTitle.setText(R.string.title_connecting);*/
	                    break;
	                case BluetoothConnectionService.STATE_LISTEN:
	                case BluetoothConnectionService.STATE_NONE:
	                    /*mTitle.setText(R.string.title_not_connected);*/
	                    break;
                }
                break;
            case BluetoothMessageType.MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                String writeMessage = new String(writeBuf);
                
                l_obj.add(Constants.KEY_MESSAGE, writeMessage);
                
                break;
            case BluetoothMessageType.MESSAGE_READ:
                //byte[] readBuf = (byte[]) msg.obj;
                BluetoothManager.OnDataReceived((byte[])msg.obj);
                // construct a string from the valid bytes in the buffer
                /*Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "Read byte length: " + readBuf.length);
                String readMessage = new String(readBuf, 0, msg.arg1);
                Log.i(Constants.TAG_BLUETOOTH_CONNECTION_HANDLER, "Read: '"+ readMessage + "'");
                l_obj.add(Constants.KEY_MESSAGE, readMessage);*/
                break;
            case BluetoothMessageType.MESSAGE_DEVICE_NAME:
                // save the connected device's name
                /*mConnectedDeviceName = msg.getData().getString(BluetoothConnectionService.DEVICE_NAME);
                Toast.makeText(s_context, "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();*/
            	//BluetoothManager.ShowToast("Connected To: " + msg.getData().getString(BluetoothConnectionService.DEVICE_NAME));
                break;
            case BluetoothMessageType.MESSAGE_TOAST:
            	/*int l_tmsg = msg.getData().getInt(BluetoothConnectionService.TOAST);
            	l_obj.add(Constants.KEY_TOAST_MSG_TYPE, l_tmsg);*/
            	//BluetoothManager.ShowToast("Showing Toast");
            	//BluetoothManager.ShowToast(msg.getData().getString(BluetoothConnectionService.TOAST));
                /*Toast.makeText(s_context, msg.getData().getString(BluetoothConnectionService.TOAST),
                               Toast.LENGTH_SHORT).show();*/
            	break;
            case BluetoothMessageType.MESSAGE_CONNECTION_GONE:
            	int l_tmsg = msg.getData().getInt(BluetoothConnectionService.CONNECTION_GONE);
            	l_obj.add(Constants.KEY_CONNECTION_GONE_TYPE, l_tmsg);
            	
                break;
            }
            
            BluetoothManager.OnMessageTypeReceived(l_obj);
        }
    };   
	
}
