package com.microeyes.bluetooth;

import com.microeyes.bluetooth.utilities.Constants;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BluetoothStateChangeBoardcastReceiver extends BroadcastReceiver {
	 
	 @Override
	 public void onReceive(Context context, Intent intent) {
		 Log.i(Constants.TAG_BLUETOOTH_STATE_CHANGE_RECEIVER, "OnReceived");
		 
	  int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
	  
	  Log.i(Constants.TAG_BLUETOOTH_STATE_CHANGE_RECEIVER, "State: " + state);
	  
	  switch(state){
		  case BluetoothAdapter.STATE_CONNECTED:
		  case BluetoothAdapter.STATE_CONNECTING:
		  case BluetoothAdapter.STATE_DISCONNECTED:
		  case BluetoothAdapter.STATE_DISCONNECTING:
		  case BluetoothAdapter.STATE_OFF:
		  case BluetoothAdapter.STATE_ON:
		  case BluetoothAdapter.STATE_TURNING_OFF:
		  case BluetoothAdapter.STATE_TURNING_ON:
			  BluetoothManager.OnBluetoothStateChanged(state);			  
			  break; 
	  }
	  
	  /*switch(state){
	  case BluetoothAdapter.STATE_CONNECTED:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_CONNECTED", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_CONNECTING:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_CONNECTING", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_DISCONNECTED:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_DISCONNECTED", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_DISCONNECTING:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_DISCONNECTING", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_OFF:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_OFF", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_ON:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_ON", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_TURNING_OFF:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_TURNING_OFF", 
	    Toast.LENGTH_SHORT).show();
	   break;
	  case BluetoothAdapter.STATE_TURNING_ON:
	   Toast.makeText(context, 
	    "BTStateChangedBroadcastReceiver: STATE_TURNING_ON", 
	    Toast.LENGTH_SHORT).show();
	   break; 
	  }*/
	 }
}
