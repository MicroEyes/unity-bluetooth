package com.microeyes.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BluetoothScanDeviceBoardcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		            // When discovery finds a device
        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
             // Get the BluetoothDevice object from the Intent
             BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
             // add the name and the MAC address of the object to the arrayAdapter
             
             BluetoothManager.OnFoundNewDevice(device);             
        } 
        else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
        	BluetoothManager.OnBluetoothDiscoveryStarted();
        }
        else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
        	BluetoothManager.OnBluetoothDiscoveryFinished();
        }

	}

}
