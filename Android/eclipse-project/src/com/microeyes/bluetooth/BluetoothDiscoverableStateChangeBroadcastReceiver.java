package com.microeyes.bluetooth;

import com.microeyes.bluetooth.utilities.Constants;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BluetoothDiscoverableStateChangeBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context arg0, Intent intent) {
		// TODO Auto-generated method stub
		
		Log.i(Constants.TAG_BLUETOOTH_DISCVR_STATE_CHANGE_RECEIVER, "OnReceived");

		int state = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, -1);
		  
		Log.i(Constants.TAG_BLUETOOTH_DISCVR_STATE_CHANGE_RECEIVER, "OnReceived:: State: " + state);
		
		switch(state){
		  case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
		  case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
		  case BluetoothAdapter.SCAN_MODE_NONE:
			  BluetoothManager.OnBluetoothDiscoverableStateChanged(state);			  
			  break; 
	  }
		
	}
}
