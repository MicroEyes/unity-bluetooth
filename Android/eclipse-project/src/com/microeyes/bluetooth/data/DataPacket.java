package com.microeyes.bluetooth.data;

public class DataPacket {
	private byte[] m_data;
	public void setData(byte[] a_data) {
		m_data = a_data;
	}
	public byte[] getData() {
		return m_data;
	}
	
	private boolean m_isReflected = false;
	public void setIsReflected(boolean a_isReflected) {
		m_isReflected  = a_isReflected;
	}
	public boolean IsReflected() {
		return m_isReflected;
	}
	
	public DataPacket(byte[] a_data, boolean a_isReflect) {
		m_data = a_data;
		m_isReflected = a_isReflect;
	}
}
