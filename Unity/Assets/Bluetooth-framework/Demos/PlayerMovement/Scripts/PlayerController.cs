﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PlayerController : MonoBehaviour
{
    #region Variables - GUI
    [SerializeField]
    private Rect m_rectBtnLeft = new Rect();

    [SerializeField]
    private Rect m_rectBtnRight = new Rect();

    [SerializeField]
    private Rect m_rectBtnUp = new Rect();

    #endregion

    void OnGUI()
    {
        if(GUI.RepeatButton(GUIUtilityEx.Unnormalize(m_rectBtnLeft), "L", "button"))
        {
            GetComponent<Rigidbody>().AddRelativeForce(Vector3.left * m_force);
        }

        if (GUI.RepeatButton(GUIUtilityEx.Unnormalize(m_rectBtnRight), "R", "button"))
        {
            GetComponent<Rigidbody>().AddRelativeForce(Vector3.right * m_force);
        }

        if (GUI.RepeatButton(GUIUtilityEx.Unnormalize(m_rectBtnUp), "U", "button"))
        {
            GetComponent<Rigidbody>().AddRelativeForce(Vector3.up * m_force);
        }
    }

    [SerializeField]
    private Vector3 m_direction = Vector3.zero;

    [SerializeField]
    private float m_force = 10.0f;


    //[SerializeField]
    //private int m_FPS = 30;

    [SerializeField]
    private float m_timeToSendPosition = 0.0f;

    [SerializeField]
    private float m_currentTimeToSendPosition = 0.0f;

    void Start()
    {
        m_timeToSendPosition = 1 /(float) GameManager.FPS_Player_Sync;
    }

    void Update()
    {
        if(m_currentTimeToSendPosition > m_timeToSendPosition)
        {
            SendPosition();
            m_currentTimeToSendPosition -= m_timeToSendPosition;
        }

        m_currentTimeToSendPosition += Time.deltaTime;
    }

    private void SendPosition()
    {
        NetworkHandler.Instance.SendLocalPlayerPosition(transform.position);
    }

}
