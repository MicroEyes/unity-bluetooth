﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using com.microeyes.bluetooth;
using com.microeyes.Data;

//[ExecuteInEditMode]
public class NetworkHandler : MonoBehaviour {

    private static NetworkHandler s_instance = null;
    public static NetworkHandler Instance
    {
        get { return s_instance; }
    }

    [SerializeField]
    private string m_uuid = string.Empty;

    void Awake()
    {
        s_instance = this;
    }

    [SerializeField]
    private Rect m_rectBtnEnable = new Rect();

    [SerializeField]
    private Rect m_rectBtnEnableDiscoverable = new Rect();

    [SerializeField]
    private Rect m_rectBtnStartListen = new Rect();

    [SerializeField]
    private Rect m_rectBtnConnect = new Rect();

    [SerializeField]
    private Rect m_rectBtnGetAllDevices = new Rect();

    [SerializeField]
    private Rect m_rectBtnStartGame = new Rect();

    [SerializeField]
    private Rect m_rectBtnStartDiscovery = new Rect();

    [SerializeField]
    List<BluetoothDevice> m_devicesList = new List<BluetoothDevice>();

    [SerializeField]
    bool m_isShowBtnStartGame = false;

    [SerializeField]
    bool m_isConnectionEstablished = false;

    //[SerializeField]
    //private Rect m_rectLogs = new Rect();

    //[SerializeField]
    //private Rect m_btnDeleteLogs = new Rect();

    //public List<string> LsttLogs = new List<string>();
    //private string StrLogs
    //{
    //    get 
    //    {
    //        StringBuilder l_builder = new StringBuilder(string.Empty);
    //        foreach (string item in LsttLogs)
    //        {
    //            l_builder.Append("\n").Append(item);
    //        }

    //        return l_builder.ToString();
    //    }
    //}


    void OnGUI()
    {
        //if(!BluetoothManager.IsInitialized)
        //{
        //    return;
        //}

        if (m_isShowBtnStartGame && m_isShowBtnStartGame)
        {
            if (BluetoothManager.IsServer)
            {
                if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnStartGame), "Start Game", "button"))
                {
                    m_isShowBtnStartGame = false;
                    OnClickedToStartGame();
                }
            }
            else
            {
                GUI.Box(GUIUtilityEx.Unnormalize(m_rectBtnStartGame), "Waiting to start game from server", "box");
            }
        }

        if (BluetoothManager.RemoteDevice != null)
            return;

        //GUI.Label(GUIUtilityEx.Unnormalize(m_rectLogs), StrLogs, "textfield");
        //if (GUI.Button(GUIUtilityEx.Unnormalize(m_btnDeleteLogs), "---" , "button"))
        //{
        //    if (LsttLogs.Count > 0)
        //    {
        //        LsttLogs.RemoveAt(0);
        //    }
        //}

        if (!m_isConnectionEstablished)
        {
            bool l_isEnabled = GUI.enabled;
            GUI.enabled = !BluetoothManager.IsEnable && l_isEnabled;
            if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnEnable), "Enable BT", "button"))
            {
                if (!BluetoothManager.IsEnable)
                {
                    BluetoothManager.Enable();
                }
                else
                {
                    Debug.Log("Already enabled");
                }
            }

            GUI.enabled = !BluetoothManager.IsDiscoverable && l_isEnabled;
            if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnEnableDiscoverable), "Turn ON Discoverable BT", "button"))
            {
                if (!BluetoothManager.IsDiscoverable)
                {
                    BluetoothManager.EnableDiscoverable(BluetoothManager.DISCOVERABLE_TIME_MAX);
                }
                else
                {
                    Debug.Log("Already in discoverable mode");
                }
            }

            GUI.enabled = BluetoothManager.IsEnable && l_isEnabled;
            if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnStartDiscovery), "Scan", "button"))
            {
                if(!BluetoothManager.IsDiscovering)
                {
                    BluetoothManager.StartDiscovery();
                }
            }

            GUI.enabled = BluetoothManager.IsEnable && !BluetoothManager.IsStartedListeningAsServer && l_isEnabled;
            if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnStartListen), "Start Listen", "button"))
            {
                StartListen();
            }

            GUI.enabled = BluetoothManager.IsEnable && l_isEnabled;
            if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnGetAllDevices), "Get All Devices", "button"))
            {
                m_devicesList = BluetoothManager.GetAllDevices();
            }

            GUI.enabled = BluetoothManager.IsEnable && (BluetoothManager.RemoteDevice == null) && (m_selectedDeviceToConnect > -1) && l_isEnabled;
            if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnConnect), "Connect", "button"))
            {
                BluetoothManager.Connect(m_devicesList[m_selectedDeviceToConnect]);
            }            

            GUI.enabled = BluetoothManager.IsEnable && l_isEnabled;
            DrawDevices();
            GUI.enabled = l_isEnabled;
        }

    }

    [SerializeField]
    private Rect m_rectScrollView = new Rect();

    [SerializeField]
    private Rect m_rectScrollViewRect = new Rect();

    [SerializeField]
    private Vector2 m_scrollPosition = Vector2.zero;

    [SerializeField]
    private Rect m_rectBtnDevice = new Rect();

    private int m_selectedDeviceToConnect = -1;

    public const string Cmd_PlayerPosition = "player_pos";
    public const string Cmd_OnStartGameByServer = "on_start_game";
    
    void DrawDevices()
    {
        GUI.BeginScrollView(GUIUtilityEx.Unnormalize(m_rectScrollView), m_scrollPosition, GUIUtilityEx.Unnormalize(m_rectScrollViewRect), true, true);

        float y = 0;
        for (int i = 0; i < m_devicesList.Count; i++)
        {
            BluetoothDevice l_device = m_devicesList[i];
            Rect l_rect = GUIUtilityEx.Unnormalize(m_rectBtnDevice);
            l_rect.y = y;

            string l_style = (m_selectedDeviceToConnect == i) ? "box" : "button";

            if (GUI.Button(l_rect, i + ". " + l_device.Name + "..." + l_device.Address, l_style))
            {
                Debug.Log("Selected: " + m_devicesList[i].ToString());
                m_selectedDeviceToConnect = i;
            }

            y += (l_rect.height + 10.0f);
        }
        

        GUI.EndScrollView();
    }

	// Use this for initialization
	void Start () {
        
        BluetoothManager.Initialize(m_uuid, (BluetoothLog.ERROR | BluetoothLog.WARNING | BluetoothLog.INFO), BluetoothLog.DEBUG_MEDIUM);

        if (!BluetoothManager.IsSupported)
        {
            Debug.LogError("Bluetooth not supported on this device.");
            return;
        }

        RegisterEvents();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    void OnApplicationQuit()
    {
        BluetoothManager.Stop();
    }

    void StartListen()
    {
        BluetoothManager.StartListening();
    }


    void RegisterEvents()
    {
        Debug.Log("Registering Events");

        BluetoothManager.OnEnabled = BT_OnEnabled;
        BluetoothManager.OnDiscoverableEnabled = BT_OnDiscoverableEnabled;

        BluetoothManager.OnConnectionEstablished = BT_OnConnectionEstablished;
        BluetoothManager.OnConnectionFailed = BT_OnConnectionFailed;
        BluetoothManager.OnConnectionLost = BT_OnConnectionLost;

        BluetoothManager.OnNewDeviceDiscovered = BT_OnNewDeviceDiscovered;


        BluetoothManager.AddResponseHandler(Cmd_PlayerPosition, OnReceived_PlayerPosition);
        BluetoothManager.AddResponseHandler(Cmd_OnStartGameByServer, OnReceived_OnStartGameByServer);
    }

    void UnRegisterEvents()
    {

    }

    #region Bluetooth Callbacks

    void LoggerCallbacks(string a_logs)
    {
        Logger.Log(a_logs);
    }

    void BT_OnEnabled(bool a_isEnabled)
    {
        if(a_isEnabled)
        {
            LoggerCallbacks("Bluetooth enabled by local user.");
            Debug.Log("Bluetooth enabled by local user.");
        }
        else
        {
            LoggerCallbacks("Enable Bluetooth denied by local user.");
            Debug.LogWarning("Enable Bluetooth denied by local user.");
        }
    }

    void BT_OnDiscoverableEnabled(bool a_isEnabled)
    {
        if(a_isEnabled)
        {
            LoggerCallbacks("Bluetooth Discoverable Turn ON by local user.");
            Debug.Log("Bluetooth Discoverable Turn ON by local user.");
        }
        else
        {
            LoggerCallbacks("Discoverable Bluetooth denied by local user.");
            Debug.LogWarning("Discoverable Bluetooth denied by local user.");
        }
    }

    void BT_OnConnectionEstablished(BluetoothDevice a_remoteDevice)
    {
        LoggerCallbacks("Connection Established successfully");
        Debug.Log("Connection Established successfully");

        m_isShowBtnStartGame = true;
        m_isConnectionEstablished = true;
        /*if(BluetoothManager.IsServer)
        {
            GameManager.Instance.SpawnLocalPlayer();
        }
        else
        {
            GameManager.Instance.SpawnRemotePlayer();
        }*/
    }

    void BT_OnConnectionFailed(BluetoothDevice a_remoteDevice)
    {
        LoggerCallbacks("Failed to connect device.");
        Debug.LogWarning("Failed to connect device.");
        m_isConnectionEstablished = false;
        GameManager.Instance.DestroyBothPlayers();
    }

    void BT_OnConnectionLost(BluetoothDevice a_remoteDevice)
    {
        LoggerCallbacks("Connection Lost to remote device.");
        Debug.LogWarning("Connection Lost to remote device.");
        m_isConnectionEstablished = false;
        GameManager.Instance.DestroyBothPlayers();
    }

    void BT_OnNewDeviceDiscovered(BluetoothDevice a_newDevice)
    {
        LoggerCallbacks("NewDevice Found: " + a_newDevice.ToString());
        Debug.Log("NewDevice Found: " + a_newDevice.ToString());
        m_devicesList.Add(a_newDevice);
    }

    #endregion

    void OnClickedToStartGame()
    {
        GameManager.Instance.SpawnLocalPlayer();
        GameManager.Instance.SpawnRemotePlayer();
        IDataObject l_obj = new DataObject();
        l_obj.PutBool("is_start", true);

        BluetoothManager.Send(Cmd_OnStartGameByServer, l_obj);
    }

    internal void SendLocalPlayerPosition(Vector3 a_position)
    {
        IDataObject l_obj = new DataObject();
        l_obj.PutFloat("pos_x", a_position.x);
        l_obj.PutFloat("pos_y", a_position.y);
        l_obj.PutFloat("pos_z", a_position.z);

        BluetoothManager.Send(Cmd_PlayerPosition, l_obj);
    }

    void OnReceived_PlayerPosition(IDataObject a_data)
    {
        IDataObject l_userData = a_data.GetDataObject(BluetoothManager.KEY_USER_DATA);
        Vector3 l_remotePlayerPosition = Vector3.zero;
        l_remotePlayerPosition.x = l_userData.GetFloat("pos_x");
        l_remotePlayerPosition.y = l_userData.GetFloat("pos_y");
        l_remotePlayerPosition.z = l_userData.GetFloat("pos_z");

        GameManager.Instance.ChangeRemotePlayerPosition(l_remotePlayerPosition);
    }

    void OnReceived_OnStartGameByServer(IDataObject a_data)
    {
        LoggerCallbacks("OnReceived_OnStartGameByServer");
        Debug.Log("OnReceived_OnStartGameByServer");
        m_isShowBtnStartGame = false;
        IDataObject l_userData = a_data.GetDataObject(BluetoothManager.KEY_USER_DATA);

        if(!BluetoothManager.IsServer)
        {
            GameManager.Instance.SpawnLocalPlayer();
            GameManager.Instance.SpawnRemotePlayer();
        }

    }
}
