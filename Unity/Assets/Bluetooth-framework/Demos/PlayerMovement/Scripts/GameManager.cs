﻿using UnityEngine;
using System.Collections;
using com.microeyes.bluetooth;

public class GameManager : MonoBehaviour {

    public static GameManager Instance = null;

    [SerializeField]
    private GameObject m_prefabLocalPlayerParent = null;

    [SerializeField]
    private GameObject m_prefabBluePlayer = null;

    [SerializeField]
    private GameObject m_prefabRedPlayer = null;

    [SerializeField]
    private Transform m_spawnPositionLocalPlayer = null;

    [SerializeField]
    private Transform m_spawnPositionRemotePlayer = null;

    public GameObject LocalPlayerParentGO = null;
    public GameObject LocalPlayerGO = null;
    public GameObject RemotePlayerGO = null;


    [SerializeField]
    private float m_FPS_PlayerSync = 0.0f;
    public static float FPS_Player_Sync
    {
        get { return Instance.m_FPS_PlayerSync; }
    }

    void Awake()
    {
        Instance = this;
    }
	
    public void SpawnLocalPlayer()
    {
        LocalPlayerParentGO = (GameObject)Instantiate(m_prefabLocalPlayerParent, m_spawnPositionLocalPlayer.position, m_spawnPositionLocalPlayer.rotation);

        GameObject l_prefabToSpawn = BluetoothManager.IsServer ? m_prefabBluePlayer : m_prefabRedPlayer;
        LocalPlayerGO = (GameObject)Instantiate(l_prefabToSpawn, m_spawnPositionLocalPlayer.position, m_spawnPositionLocalPlayer.rotation);

        LocalPlayerGO.transform.parent = LocalPlayerParentGO.transform;
        LocalPlayerGO.transform.localPosition = Vector3.zero;
    }

    public void SpawnRemotePlayer()
    {
        GameObject l_prefabToSpawn = BluetoothManager.IsServer ? m_prefabRedPlayer : m_prefabBluePlayer;
        RemotePlayerGO = (GameObject)Instantiate(l_prefabToSpawn, m_spawnPositionRemotePlayer.position, m_spawnPositionRemotePlayer.rotation);
    }

    internal void ChangeRemotePlayerPosition(Vector3 l_remotePlayerPosition)
    {
        RemotePlayerGO.transform.position = l_remotePlayerPosition;
    }

    public void DestroyBothPlayers()
    {
#if UNITY_EDITOR
        GameObject.DestroyImmediate(LocalPlayerGO);
        GameObject.DestroyImmediate(RemotePlayerGO);
#else
        GameObject.Destroy(LocalPlayerGO);
        GameObject.Destroy(RemotePlayerGO);
#endif
    }
}
