﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

[ExecuteInEditMode]
public class Logger : MonoBehaviour {

    private static Logger s_instance = null;

    [SerializeField]
    private GUISkin m_skin = null;

    [SerializeField]
    private bool m_isExpanded = false;

    [SerializeField]
    private Rect m_rectBtnLogger = new Rect();
    
    [SerializeField]
    private Rect m_rectBtnDelete = new Rect();

    [SerializeField]
    private Rect m_rectScrollView = new Rect();

    [SerializeField]
    private Rect m_rectScrollViewContent = new Rect();

    [SerializeField]
    Vector2 m_scrollPosition = Vector2.zero;

    [SerializeField]
    private Rect m_rectTextArea = new Rect();

    [SerializeField]
    private List<string> m_logs = new List<string>();

    string Logs
    {
        get
        {
            StringBuilder l_logs = new StringBuilder();

            foreach (string item in m_logs)
            {
                l_logs.Append("-- ").Append(item).Append("\n");
            }

            return l_logs.ToString();
        }
    }

    void Awake()
    {
        s_instance = this;
    }

    void OnGUI()
    {
        GUISkin l_tempSkin = GUI.skin;
        GUI.skin = m_skin;

        if(GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnLogger), "Exp/Col", "button"))
        {
            m_isExpanded = !m_isExpanded;
        }

        if (GUI.Button(GUIUtilityEx.Unnormalize(m_rectBtnDelete), "----", "button"))
        {
            if(m_logs.Count >0 )
            {
                m_logs.RemoveAt(0);
            }
        }

        if(m_isExpanded)
        {
            m_scrollPosition = GUI.BeginScrollView(GUIUtilityEx.UnnormizePosition(m_rectScrollViewContent), m_scrollPosition, GUIUtilityEx.UnnormizePosition(m_rectScrollView));

            m_rectTextArea.height = GUIUtilityEx.GetHeight(m_skin, "textarea", m_rectTextArea.width, Logs);
            GUI.TextArea(m_rectTextArea, Logs);

            m_rectScrollView.height = m_rectTextArea.height + 20.0f;

            GUI.EndScrollView();
        }

        GUI.skin = l_tempSkin;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void Log(string a_str)
    {
        s_instance.m_logs.Add(a_str);
    }
}
