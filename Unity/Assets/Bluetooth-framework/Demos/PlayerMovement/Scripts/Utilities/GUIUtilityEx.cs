﻿using UnityEngine;
using System.Collections;

public class GUIUtilityEx {

    public static Rect Unnormalize(Rect a_rect)
    {
        Rect l_rect = new Rect();
        l_rect.x = a_rect.x * Screen.width;
        l_rect.y = a_rect.y * Screen.height;
        l_rect.width = a_rect.width * Screen.width;
        l_rect.height = a_rect.height * Screen.height;
        return l_rect;
    }

    public static Rect UnnormizePosition(Rect a_rect)
    {
        Rect l_rect = new Rect();
        l_rect.x = a_rect.x * Screen.width; 
        l_rect.y = a_rect.y * Screen.height;
        l_rect.width = a_rect.width;
        l_rect.height = a_rect.height;
        return l_rect;
    }

    public static float GetHeight(GUISkin a_skin, string a_style, float a_width, string a_text)
    {
        float l_height = 0.0f;
        GUIStyle l_style = a_skin.GetStyle(a_style);
        GUIContent l_content = new GUIContent(a_text);
        l_height = l_style.CalcHeight(l_content, a_width);
        return l_height;
    }
}
